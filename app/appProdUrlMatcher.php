<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // bloomUser_home
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'bloomUser_home');
            }

            return array (  '_controller' => 'Blog\\ArticleBundle\\Controller\\indexController::indexAction',  '_route' => 'bloomUser_home',);
        }

        if (0 === strpos($pathinfo, '/fiche_candidature')) {
            // tunisie_candidature
            if ($pathinfo === '/fiche_candidature/create') {
                return array (  '_controller' => 'Blog\\ArticleBundle\\Controller\\indexController::candidatureAction',  '_route' => 'tunisie_candidature',);
            }

            // tunisie_candidature_success
            if (0 === strpos($pathinfo, '/fiche_candidature/success') && preg_match('#^/fiche_candidature/success/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'tunisie_candidature_success')), array (  '_controller' => 'Blog\\ArticleBundle\\Controller\\indexController::candidaturesuccessAction',));
            }

            // tunisie_candidature_folder
            if ($pathinfo === '/fiche_candidature/folder') {
                return array (  '_controller' => 'Blog\\ArticleBundle\\Controller\\indexController::candidaturefolderAction',  '_route' => 'tunisie_candidature_folder',);
            }

        }

        // fos_js_routing_js
        if (0 === strpos($pathinfo, '/js/routing') && preg_match('#^/js/routing(?:\\.(?P<_format>js|json))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_js_routing_js')), array (  '_controller' => 'fos_js_routing.controller:indexAction',  '_format' => 'js',));
        }

        if (0 === strpos($pathinfo, '/admin')) {
            // sonata_admin_redirect
            if (rtrim($pathinfo, '/') === '/admin') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'sonata_admin_redirect');
                }

                return array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController::redirectAction',  'route' => 'sonata_admin_dashboard',  'permanent' => 'true',  '_route' => 'sonata_admin_redirect',);
            }

            // sonata_admin_dashboard
            if ($pathinfo === '/admin/dashboard') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::dashboardAction',  '_route' => 'sonata_admin_dashboard',);
            }

            if (0 === strpos($pathinfo, '/admin/core')) {
                // sonata_admin_retrieve_form_element
                if ($pathinfo === '/admin/core/get-form-field-element') {
                    return array (  '_controller' => 'sonata.admin.controller.admin:retrieveFormFieldElementAction',  '_route' => 'sonata_admin_retrieve_form_element',);
                }

                // sonata_admin_append_form_element
                if ($pathinfo === '/admin/core/append-form-field-element') {
                    return array (  '_controller' => 'sonata.admin.controller.admin:appendFormFieldElementAction',  '_route' => 'sonata_admin_append_form_element',);
                }

                // sonata_admin_short_object_information
                if (0 === strpos($pathinfo, '/admin/core/get-short-object-description') && preg_match('#^/admin/core/get\\-short\\-object\\-description(?:\\.(?P<_format>html|json))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_short_object_information')), array (  '_controller' => 'sonata.admin.controller.admin:getShortObjectDescriptionAction',  '_format' => 'html',));
                }

                // sonata_admin_set_object_field_value
                if ($pathinfo === '/admin/core/set-object-field-value') {
                    return array (  '_controller' => 'sonata.admin.controller.admin:setObjectFieldValueAction',  '_route' => 'sonata_admin_set_object_field_value',);
                }

            }

            // sonata_admin_search
            if ($pathinfo === '/admin/search') {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::searchAction',  '_route' => 'sonata_admin_search',);
            }

            // sonata_admin_retrieve_autocomplete_items
            if ($pathinfo === '/admin/core/get-autocomplete-items') {
                return array (  '_controller' => 'sonata.admin.controller.admin:retrieveAutocompleteItemsAction',  '_route' => 'sonata_admin_retrieve_autocomplete_items',);
            }

            if (0 === strpos($pathinfo, '/admin/blog/article')) {
                if (0 === strpos($pathinfo, '/admin/blog/article/pays')) {
                    // admin_blog_article_pays_list
                    if ($pathinfo === '/admin/blog/article/pays/list') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.pays',  '_sonata_name' => 'admin_blog_article_pays_list',  '_route' => 'admin_blog_article_pays_list',);
                    }

                    // admin_blog_article_pays_create
                    if ($pathinfo === '/admin/blog/article/pays/create') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.pays',  '_sonata_name' => 'admin_blog_article_pays_create',  '_route' => 'admin_blog_article_pays_create',);
                    }

                    // admin_blog_article_pays_batch
                    if ($pathinfo === '/admin/blog/article/pays/batch') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.pays',  '_sonata_name' => 'admin_blog_article_pays_batch',  '_route' => 'admin_blog_article_pays_batch',);
                    }

                    // admin_blog_article_pays_edit
                    if (preg_match('#^/admin/blog/article/pays/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_pays_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.pays',  '_sonata_name' => 'admin_blog_article_pays_edit',));
                    }

                    // admin_blog_article_pays_delete
                    if (preg_match('#^/admin/blog/article/pays/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_pays_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.pays',  '_sonata_name' => 'admin_blog_article_pays_delete',));
                    }

                    // admin_blog_article_pays_show
                    if (preg_match('#^/admin/blog/article/pays/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_pays_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.pays',  '_sonata_name' => 'admin_blog_article_pays_show',));
                    }

                    // admin_blog_article_pays_export
                    if ($pathinfo === '/admin/blog/article/pays/export') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.pays',  '_sonata_name' => 'admin_blog_article_pays_export',  '_route' => 'admin_blog_article_pays_export',);
                    }

                }

                if (0 === strpos($pathinfo, '/admin/blog/article/annee')) {
                    // admin_blog_article_annee_list
                    if ($pathinfo === '/admin/blog/article/annee/list') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.annee',  '_sonata_name' => 'admin_blog_article_annee_list',  '_route' => 'admin_blog_article_annee_list',);
                    }

                    // admin_blog_article_annee_create
                    if ($pathinfo === '/admin/blog/article/annee/create') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.annee',  '_sonata_name' => 'admin_blog_article_annee_create',  '_route' => 'admin_blog_article_annee_create',);
                    }

                    // admin_blog_article_annee_batch
                    if ($pathinfo === '/admin/blog/article/annee/batch') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.annee',  '_sonata_name' => 'admin_blog_article_annee_batch',  '_route' => 'admin_blog_article_annee_batch',);
                    }

                    // admin_blog_article_annee_edit
                    if (preg_match('#^/admin/blog/article/annee/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_annee_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.annee',  '_sonata_name' => 'admin_blog_article_annee_edit',));
                    }

                    // admin_blog_article_annee_delete
                    if (preg_match('#^/admin/blog/article/annee/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_annee_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.annee',  '_sonata_name' => 'admin_blog_article_annee_delete',));
                    }

                    // admin_blog_article_annee_show
                    if (preg_match('#^/admin/blog/article/annee/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_annee_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.annee',  '_sonata_name' => 'admin_blog_article_annee_show',));
                    }

                    // admin_blog_article_annee_export
                    if ($pathinfo === '/admin/blog/article/annee/export') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.annee',  '_sonata_name' => 'admin_blog_article_annee_export',  '_route' => 'admin_blog_article_annee_export',);
                    }

                }

                if (0 === strpos($pathinfo, '/admin/blog/article/c')) {
                    if (0 === strpos($pathinfo, '/admin/blog/article/cursus')) {
                        // admin_blog_article_cursus_list
                        if ($pathinfo === '/admin/blog/article/cursus/list') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.cursus',  '_sonata_name' => 'admin_blog_article_cursus_list',  '_route' => 'admin_blog_article_cursus_list',);
                        }

                        // admin_blog_article_cursus_create
                        if ($pathinfo === '/admin/blog/article/cursus/create') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.cursus',  '_sonata_name' => 'admin_blog_article_cursus_create',  '_route' => 'admin_blog_article_cursus_create',);
                        }

                        // admin_blog_article_cursus_batch
                        if ($pathinfo === '/admin/blog/article/cursus/batch') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.cursus',  '_sonata_name' => 'admin_blog_article_cursus_batch',  '_route' => 'admin_blog_article_cursus_batch',);
                        }

                        // admin_blog_article_cursus_edit
                        if (preg_match('#^/admin/blog/article/cursus/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_cursus_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.cursus',  '_sonata_name' => 'admin_blog_article_cursus_edit',));
                        }

                        // admin_blog_article_cursus_delete
                        if (preg_match('#^/admin/blog/article/cursus/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_cursus_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.cursus',  '_sonata_name' => 'admin_blog_article_cursus_delete',));
                        }

                        // admin_blog_article_cursus_show
                        if (preg_match('#^/admin/blog/article/cursus/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_cursus_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.cursus',  '_sonata_name' => 'admin_blog_article_cursus_show',));
                        }

                        // admin_blog_article_cursus_export
                        if ($pathinfo === '/admin/blog/article/cursus/export') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.cursus',  '_sonata_name' => 'admin_blog_article_cursus_export',  '_route' => 'admin_blog_article_cursus_export',);
                        }

                    }

                    if (0 === strpos($pathinfo, '/admin/blog/article/candid')) {
                        // admin_blog_article_candid_list
                        if ($pathinfo === '/admin/blog/article/candid/list') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.candid',  '_sonata_name' => 'admin_blog_article_candid_list',  '_route' => 'admin_blog_article_candid_list',);
                        }

                        // admin_blog_article_candid_create
                        if ($pathinfo === '/admin/blog/article/candid/create') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.candid',  '_sonata_name' => 'admin_blog_article_candid_create',  '_route' => 'admin_blog_article_candid_create',);
                        }

                        // admin_blog_article_candid_batch
                        if ($pathinfo === '/admin/blog/article/candid/batch') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.candid',  '_sonata_name' => 'admin_blog_article_candid_batch',  '_route' => 'admin_blog_article_candid_batch',);
                        }

                        // admin_blog_article_candid_edit
                        if (preg_match('#^/admin/blog/article/candid/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_candid_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.candid',  '_sonata_name' => 'admin_blog_article_candid_edit',));
                        }

                        // admin_blog_article_candid_delete
                        if (preg_match('#^/admin/blog/article/candid/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_candid_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.candid',  '_sonata_name' => 'admin_blog_article_candid_delete',));
                        }

                        // admin_blog_article_candid_show
                        if (preg_match('#^/admin/blog/article/candid/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_candid_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.candid',  '_sonata_name' => 'admin_blog_article_candid_show',));
                        }

                        // admin_blog_article_candid_export
                        if ($pathinfo === '/admin/blog/article/candid/export') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.candid',  '_sonata_name' => 'admin_blog_article_candid_export',  '_route' => 'admin_blog_article_candid_export',);
                        }

                    }

                }

                if (0 === strpos($pathinfo, '/admin/blog/article/slide')) {
                    // admin_blog_article_slide_list
                    if ($pathinfo === '/admin/blog/article/slide/list') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.slide',  '_sonata_name' => 'admin_blog_article_slide_list',  '_route' => 'admin_blog_article_slide_list',);
                    }

                    // admin_blog_article_slide_create
                    if ($pathinfo === '/admin/blog/article/slide/create') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.slide',  '_sonata_name' => 'admin_blog_article_slide_create',  '_route' => 'admin_blog_article_slide_create',);
                    }

                    // admin_blog_article_slide_batch
                    if ($pathinfo === '/admin/blog/article/slide/batch') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.slide',  '_sonata_name' => 'admin_blog_article_slide_batch',  '_route' => 'admin_blog_article_slide_batch',);
                    }

                    // admin_blog_article_slide_edit
                    if (preg_match('#^/admin/blog/article/slide/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_slide_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.slide',  '_sonata_name' => 'admin_blog_article_slide_edit',));
                    }

                    // admin_blog_article_slide_delete
                    if (preg_match('#^/admin/blog/article/slide/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_slide_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.slide',  '_sonata_name' => 'admin_blog_article_slide_delete',));
                    }

                    // admin_blog_article_slide_show
                    if (preg_match('#^/admin/blog/article/slide/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_slide_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.slide',  '_sonata_name' => 'admin_blog_article_slide_show',));
                    }

                    // admin_blog_article_slide_export
                    if ($pathinfo === '/admin/blog/article/slide/export') {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.slide',  '_sonata_name' => 'admin_blog_article_slide_export',  '_route' => 'admin_blog_article_slide_export',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/admin/sonata/media')) {
                if (0 === strpos($pathinfo, '/admin/sonata/media/media')) {
                    // admin_sonata_media_media_list
                    if ($pathinfo === '/admin/sonata/media/media/list') {
                        return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::listAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_list',  '_route' => 'admin_sonata_media_media_list',);
                    }

                    // admin_sonata_media_media_create
                    if ($pathinfo === '/admin/sonata/media/media/create') {
                        return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::createAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_create',  '_route' => 'admin_sonata_media_media_create',);
                    }

                    // admin_sonata_media_media_batch
                    if ($pathinfo === '/admin/sonata/media/media/batch') {
                        return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::batchAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_batch',  '_route' => 'admin_sonata_media_media_batch',);
                    }

                    // admin_sonata_media_media_edit
                    if (preg_match('#^/admin/sonata/media/media/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_media_edit')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::editAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_edit',));
                    }

                    // admin_sonata_media_media_delete
                    if (preg_match('#^/admin/sonata/media/media/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_media_delete')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::deleteAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_delete',));
                    }

                    // admin_sonata_media_media_show
                    if (preg_match('#^/admin/sonata/media/media/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_media_show')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::showAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_show',));
                    }

                    // admin_sonata_media_media_export
                    if ($pathinfo === '/admin/sonata/media/media/export') {
                        return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::exportAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_export',  '_route' => 'admin_sonata_media_media_export',);
                    }

                }

                if (0 === strpos($pathinfo, '/admin/sonata/media/gallery')) {
                    // admin_sonata_media_gallery_list
                    if ($pathinfo === '/admin/sonata/media/gallery/list') {
                        return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::listAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_list',  '_route' => 'admin_sonata_media_gallery_list',);
                    }

                    // admin_sonata_media_gallery_create
                    if ($pathinfo === '/admin/sonata/media/gallery/create') {
                        return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::createAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_create',  '_route' => 'admin_sonata_media_gallery_create',);
                    }

                    // admin_sonata_media_gallery_batch
                    if ($pathinfo === '/admin/sonata/media/gallery/batch') {
                        return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::batchAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_batch',  '_route' => 'admin_sonata_media_gallery_batch',);
                    }

                    // admin_sonata_media_gallery_edit
                    if (preg_match('#^/admin/sonata/media/gallery/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_gallery_edit')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::editAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_edit',));
                    }

                    // admin_sonata_media_gallery_delete
                    if (preg_match('#^/admin/sonata/media/gallery/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_gallery_delete')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::deleteAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_delete',));
                    }

                    // admin_sonata_media_gallery_show
                    if (preg_match('#^/admin/sonata/media/gallery/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_gallery_show')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::showAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_show',));
                    }

                    // admin_sonata_media_gallery_export
                    if ($pathinfo === '/admin/sonata/media/gallery/export') {
                        return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::exportAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_export',  '_route' => 'admin_sonata_media_gallery_export',);
                    }

                    if (0 === strpos($pathinfo, '/admin/sonata/media/galleryhasmedia')) {
                        // admin_sonata_media_galleryhasmedia_list
                        if ($pathinfo === '/admin/sonata/media/galleryhasmedia/list') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_list',  '_route' => 'admin_sonata_media_galleryhasmedia_list',);
                        }

                        // admin_sonata_media_galleryhasmedia_create
                        if ($pathinfo === '/admin/sonata/media/galleryhasmedia/create') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_create',  '_route' => 'admin_sonata_media_galleryhasmedia_create',);
                        }

                        // admin_sonata_media_galleryhasmedia_batch
                        if ($pathinfo === '/admin/sonata/media/galleryhasmedia/batch') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_batch',  '_route' => 'admin_sonata_media_galleryhasmedia_batch',);
                        }

                        // admin_sonata_media_galleryhasmedia_edit
                        if (preg_match('#^/admin/sonata/media/galleryhasmedia/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_galleryhasmedia_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_edit',));
                        }

                        // admin_sonata_media_galleryhasmedia_delete
                        if (preg_match('#^/admin/sonata/media/galleryhasmedia/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_galleryhasmedia_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_delete',));
                        }

                        // admin_sonata_media_galleryhasmedia_show
                        if (preg_match('#^/admin/sonata/media/galleryhasmedia/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_galleryhasmedia_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_show',));
                        }

                        // admin_sonata_media_galleryhasmedia_export
                        if ($pathinfo === '/admin/sonata/media/galleryhasmedia/export') {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_export',  '_route' => 'admin_sonata_media_galleryhasmedia_export',);
                        }

                    }

                }

            }

        }

        if (0 === strpos($pathinfo, '/media')) {
            if (0 === strpos($pathinfo, '/media/gallery')) {
                // sonata_media_gallery_index
                if (rtrim($pathinfo, '/') === '/media/gallery') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'sonata_media_gallery_index');
                    }

                    return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryController::indexAction',  '_route' => 'sonata_media_gallery_index',);
                }

                // sonata_media_gallery_view
                if (0 === strpos($pathinfo, '/media/gallery/view') && preg_match('#^/media/gallery/view/(?P<id>.*)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_media_gallery_view')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryController::viewAction',));
                }

            }

            // sonata_media_view
            if (0 === strpos($pathinfo, '/media/view') && preg_match('#^/media/view/(?P<id>[^/]++)(?:/(?P<format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_media_view')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaController::viewAction',  'format' => 'reference',));
            }

            // sonata_media_download
            if (0 === strpos($pathinfo, '/media/download') && preg_match('#^/media/download/(?P<id>.*)(?:/(?P<format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_media_download')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaController::downloadAction',  'format' => 'reference',));
            }

        }

        // ed_blog_admin_feed
        if (0 === strpos($pathinfo, '/feed') && preg_match('#^/feed/(?P<type>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_admin_feed')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\FeedController::feedAction',));
        }

        if (0 === strpos($pathinfo, '/all-news')) {
            // ed_blog_homepage
            if (rtrim($pathinfo, '/') === '/all-news') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'ed_blog_homepage');
                }

                return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\BlogController::indexAction',  '_route' => 'ed_blog_homepage',);
            }

            if (0 === strpos($pathinfo, '/all-news/blog')) {
                // ed_blog_frontend_index
                if ($pathinfo === '/all-news/blog') {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\BlogController::indexAction',  '_route' => 'ed_blog_frontend_index',);
                }

                // ed_blog_frontend_blog_index
                if (rtrim($pathinfo, '/') === '/all-news/blog') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'ed_blog_frontend_blog_index');
                    }

                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\BlogController::indexAction',  '_route' => 'ed_blog_frontend_blog_index',);
                }

                // ed_frontend_blog_single_article
                if (preg_match('#^/all\\-news/blog/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_frontend_blog_single_article')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\BlogController::singleArticleAction',));
                }

                // ed_frontend_blog_by_category
                if (0 === strpos($pathinfo, '/all-news/blog/category') && preg_match('#^/all\\-news/blog/category/(?P<categorySlug>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_frontend_blog_by_category')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\BlogController::byCategoryAction',));
                }

                // ed_frontend_blog_by_tag
                if (0 === strpos($pathinfo, '/all-news/blog/tag') && preg_match('#^/all\\-news/blog/tag/(?P<tagSlug>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_frontend_blog_by_tag')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\BlogController::byTagAction',));
                }

                if (0 === strpos($pathinfo, '/all-news/blog/a')) {
                    // ed_frontend_blog_by_author
                    if (0 === strpos($pathinfo, '/all-news/blog/author') && preg_match('#^/all\\-news/blog/author/(?P<username>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_frontend_blog_by_author')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\BlogController::byAuthorAction',));
                    }

                    // ed_frontend_blog_archive
                    if (0 === strpos($pathinfo, '/all-news/blog/archive') && preg_match('#^/all\\-news/blog/archive/(?P<yearMonth>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_frontend_blog_archive')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\BlogController::archiveAction',));
                    }

                }

            }

            // ed_blog_redirect_authenticate
            if ($pathinfo === '/all-news/authenticate') {
                return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\RedirectController::authenticateAction',  '_route' => 'ed_blog_redirect_authenticate',);
            }

            // ed_blog_redirect_login
            if ($pathinfo === '/all-news/login' && $request->isXmlHttpRequest()) {
                return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\RedirectController::ajaxLoginAction',  '_route' => 'ed_blog_redirect_login',);
            }

        }

        if (0 === strpos($pathinfo, '/news/admin')) {
            if (0 === strpos($pathinfo, '/news/admin/article')) {
                // ed_blog_admin_article_create
                if ($pathinfo === '/news/admin/article/create') {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::createAction',  '_route' => 'ed_blog_admin_article_create',);
                }

                // ed_blog_admin_article_edit
                if (0 === strpos($pathinfo, '/news/admin/article/edit') && preg_match('#^/news/admin/article/edit/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_admin_article_edit')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::editAction',));
                }

                // ed_blog_article_autosave
                if (0 === strpos($pathinfo, '/news/admin/article/autosave') && preg_match('#^/news/admin/article/autosave/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_article_autosave')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::autosaveAction',));
                }

                // ed_blog_admin_article_media_list
                if ($pathinfo === '/news/admin/article/media/list') {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::mediaListAction',  '_route' => 'ed_blog_admin_article_media_list',);
                }

            }

            // ed_blog_admin_article_upload
            if ($pathinfo === '/news/admin/upload') {
                return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::uploadAction',  '_route' => 'ed_blog_admin_article_upload',);
            }

            if (0 === strpos($pathinfo, '/news/admin/article')) {
                // ed_blog_admin_article_list
                if ($pathinfo === '/news/admin/article/list') {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::listAction',  '_route' => 'ed_blog_admin_article_list',);
                }

                // ed_blog_admin_article_show
                if (0 === strpos($pathinfo, '/news/admin/article/show') && preg_match('#^/news/admin/article/show/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_admin_article_show')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::showAction',));
                }

                // ed_blog_admin_article_delete
                if (preg_match('#^/news/admin/article/(?P<slug>[^/]++)/remove$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_admin_article_delete')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::deleteAction',));
                }

                // ed_blog_article_check_writing_lock
                if (preg_match('#^/news/admin/article/(?P<id>[^/]++)/check$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_article_check_writing_lock')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::checkWritingLockAction',));
                }

                // ed_blog_article_takeover
                if (preg_match('#^/news/admin/article/(?P<id>[^/]++)/takeover$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_article_takeover')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::takeoverAction',));
                }

            }

            if (0 === strpos($pathinfo, '/news/admin/c')) {
                if (0 === strpos($pathinfo, '/news/admin/category')) {
                    // ed_blog_category_list
                    if ($pathinfo === '/news/admin/category/list') {
                        return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CategoryController::listAction',  '_route' => 'ed_blog_category_list',);
                    }

                    // ed_blog_category_create
                    if ($pathinfo === '/news/admin/category/create') {
                        return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CategoryController::createAction',  '_route' => 'ed_blog_category_create',);
                    }

                    // ed_blog_category_edit
                    if (preg_match('#^/news/admin/category/(?P<slug>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_category_edit')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CategoryController::editAction',));
                    }

                    // ed_blog_category_delete
                    if (preg_match('#^/news/admin/category/(?P<slug>[^/]++)/remove$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_category_delete')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CategoryController::deleteAction',));
                    }

                    // ed_blog_category_all_pretty
                    if (preg_match('#^/news/admin/category/(?P<template>[^/]++)/pretty$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_category_all_pretty')), array (  'template' => 'all',  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CategoryController::allPrettyAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/news/admin/comment')) {
                    // ed_blog_admin_comment_create
                    if (preg_match('#^/news/admin/comment/(?P<slug>[^/]++)/create$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_admin_comment_create')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CommentController::createAction',));
                    }

                    // ed_blog_admin_comment_list
                    if ($pathinfo === '/news/admin/comment/list') {
                        return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CommentController::listAction',  '_route' => 'ed_blog_admin_comment_list',);
                    }

                    // ed_blog_comment_delete
                    if (preg_match('#^/news/admin/comment/(?P<id>[^/]++)/remove$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_comment_delete')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CommentController::deleteAction',));
                    }

                    // ed_blog_comment_edit_status
                    if (preg_match('#^/news/admin/comment/(?P<id>[^/]++)/status/(?P<status>0|1)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_comment_edit_status')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CommentController::editStatusAction',));
                    }

                    // ed_blog_comment_edit
                    if (preg_match('#^/news/admin/comment/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_comment_edit')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CommentController::editAction',));
                    }

                }

            }

            // ed_blog_homepage_index
            if (rtrim($pathinfo, '/') === '/news/admin') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'ed_blog_homepage_index');
                }

                return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\HomepageController::indexAction',  '_route' => 'ed_blog_homepage_index',);
            }

            if (0 === strpos($pathinfo, '/news/admin/media')) {
                // ed_blog_admin_media_list
                if ($pathinfo === '/news/admin/media/list') {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\MediaController::mediaListAction',  '_route' => 'ed_blog_admin_media_list',);
                }

                if (0 === strpos($pathinfo, '/news/admin/media/upload')) {
                    // ed_blog_admin_media_upload
                    if ($pathinfo === '/news/admin/media/upload') {
                        return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\MediaController::uploadAction',  '_route' => 'ed_blog_admin_media_upload',);
                    }

                    // ed_blog_admin_media_upload_excerpt
                    if ($pathinfo === '/news/admin/media/upload/excerpt') {
                        return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\MediaController::uploadExcerptAction',  '_route' => 'ed_blog_admin_media_upload_excerpt',);
                    }

                }

                // ed_blog_admin_media_delete
                if (0 === strpos($pathinfo, '/news/admin/media/delete') && preg_match('#^/news/admin/media/delete/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_admin_media_delete')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\MediaController::deleteAction',));
                }

                if (0 === strpos($pathinfo, '/news/admin/media/edit')) {
                    // ed_blog_admin_media_edit_info
                    if (0 === strpos($pathinfo, '/news/admin/media/edit-info') && preg_match('#^/news/admin/media/edit\\-info/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_admin_media_edit_info')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\MediaController::editInfoAction',));
                    }

                    // ed_blog_admin_media_edit
                    if (preg_match('#^/news/admin/media/edit/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_admin_media_edit')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\MediaController::editAction',));
                    }

                }

                // ed_blog_admin_media_crop
                if (0 === strpos($pathinfo, '/news/admin/media/crop') && preg_match('#^/news/admin/media/crop/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_admin_media_crop')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\MediaController::cropAction',));
                }

            }

            if (0 === strpos($pathinfo, '/news/admin/settings')) {
                // ed_blog_admin_settings_edit
                if ($pathinfo === '/news/admin/settings/edit') {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\SettingsController::editAction',  '_route' => 'ed_blog_admin_settings_edit',);
                }

                // ed_blog_admin_settings_show_format
                if ($pathinfo === '/news/admin/settings/showDateTime') {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\SettingsController::showFormat',  '_route' => 'ed_blog_admin_settings_show_format',);
                }

            }

            if (0 === strpos($pathinfo, '/news/admin/tag')) {
                // ed_blog_tag_express_create
                if ($pathinfo === '/news/admin/tag/express-create') {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\TagController::createExpressAction',  '_route' => 'ed_blog_tag_express_create',);
                }

                // ed_blog_tag_list
                if ($pathinfo === '/news/admin/tag/list') {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\TagController::listAction',  '_route' => 'ed_blog_tag_list',);
                }

                // ed_blog_tag_create
                if ($pathinfo === '/news/admin/tag/create') {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\TagController::createAction',  '_route' => 'ed_blog_tag_create',);
                }

                // ed_blog_tag_edit
                if (preg_match('#^/news/admin/tag/(?P<slug>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_tag_edit')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\TagController::editAction',));
                }

                // ed_blog_tag_delete
                if (preg_match('#^/news/admin/tag/(?P<slug>[^/]++)/remove$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_tag_delete')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\TagController::deleteAction',));
                }

                // ed_blog_tags_top_tags
                if (0 === strpos($pathinfo, '/news/admin/tag/toptags') && preg_match('#^/news/admin/tag/toptags/(?P<number>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_tags_top_tags')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\TagController::topTagsAction',));
                }

            }

            if (0 === strpos($pathinfo, '/news/admin/user')) {
                // ed_blog_user_list
                if ($pathinfo === '/news/admin/user/list') {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\UserController::listAction',  '_route' => 'ed_blog_user_list',);
                }

                // ed_blog_user_add
                if ($pathinfo === '/news/admin/user/add') {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\UserController::addAction',  '_route' => 'ed_blog_user_add',);
                }

                // ed_blog_user_search
                if ($pathinfo === '/news/admin/user/search') {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\UserController::searchAction',  '_route' => 'ed_blog_user_search',);
                }

                // ed_blog_user_edit
                if (0 === strpos($pathinfo, '/news/admin/user/edit') && preg_match('#^/news/admin/user/edit/(?P<username>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_user_edit')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\UserController::editAction',));
                }

                // ed_blog_user_revoke
                if (preg_match('#^/news/admin/user/(?P<username>[^/]++)/revoke$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_user_revoke')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\UserController::revokeAction',));
                }

            }

        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ($pathinfo === '/login') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }
                not_fos_user_security_login:

                // fos_user_security_check
                if ($pathinfo === '/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ($pathinfo === '/logout') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_security_logout;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }
            not_fos_user_security_logout:

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ($pathinfo === '/profile/edit') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_profile_edit;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }
            not_fos_user_profile_edit:

        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/register')) {
                // fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/register') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_registration_register;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
                }
                not_fos_user_registration_register:

                if (0 === strpos($pathinfo, '/register/c')) {
                    // fos_user_registration_check_email
                    if ($pathinfo === '/register/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/register/confirm')) {
                        // fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ($pathinfo === '/register/confirmed') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/resetting')) {
                // fos_user_resetting_request
                if ($pathinfo === '/resetting/request') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_request;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
                }
                not_fos_user_resetting_request:

                // fos_user_resetting_send_email
                if ($pathinfo === '/resetting/send-email') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_resetting_send_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                }
                not_fos_user_resetting_send_email:

                // fos_user_resetting_check_email
                if ($pathinfo === '/resetting/check-email') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_check_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                }
                not_fos_user_resetting_check_email:

                // fos_user_resetting_reset
                if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_resetting_reset;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
                }
                not_fos_user_resetting_reset:

            }

        }

        // fos_user_change_password
        if ($pathinfo === '/profile/change-password') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }

            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
        }
        not_fos_user_change_password:

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
