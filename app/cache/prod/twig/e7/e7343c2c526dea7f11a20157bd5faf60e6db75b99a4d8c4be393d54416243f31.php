<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* BlogArticleBundle:Tool:footer.html.twig */
class __TwigTemplate_6e67e456d352659443d0d86b81963f6b509f42e03607c4257f1b05cad6f7bb0f extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "BlogArticleBundle:Tool:footer.html.twig"));

        // line 1
        echo "<footer class=\"footer-black footer\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"footer-body\">
                <div class=\"col-lg-4 col-md-4 col-sm-3 col-xs-12\">
                    <div class=\"_title\">A PROPOS</div>
                    <p>
                        <span class=\"main\">CDS TECHNOLOGIES Sarl</span> est une entreprise de droit Camerounais à vocation sous régionale dont la création résulte d'une réelle volonté de ses fondateurs, de disposer dans le domaine  des télécommunications au Cameroun.
                    </p>                
                </div>
                <div class=\"col-lg-4 col-md-4 col-sm-3 col-xs-12\">
                    <div class=\"_title\">LIENS UTILES</div>
                    <ul class=\"list-unstyled\">
                        <li >
                            <a href=\"";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("home");
        echo "\">
                                > Accueil
                            </a>
                        </li>
                        <li >
                            <a  href=\"";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("service");
        echo "\">
                                > Services
                            </a>
                        </li>
                        <li >
                            <a  href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("produit");
        echo "\">
                                > Produits
                            </a>
                        </li>
                        <li >
                            <a  href=\"";
        // line 30
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ed_blog_homepage");
        echo "\">
                                > Actualités
                            </a>
                        </li>
                        <li >
                            <a  href=\"";
        // line 35
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("contact");
        echo "\">
                                > Contact
                            </a>
                        </li>
                        ";
        // line 39
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_BLOG_USER")) {
            // line 40
            echo "                            <li ><a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("sonata_admin_dashboard");
            echo "\">  > Administration Generale</a></li>
                            <li ><a href=\"";
            // line 41
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\">  > Deconnexion</a></li>

                        ";
        } else {
            // line 44
            echo "                            <li ><a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\">  > Connexion</a></li>
                            ";
        }
        // line 46
        echo "                        
                    </ul>
                </div>
                <div class=\"col-lg-4 col-md-4 col-sm-3 col-xs-12\">
                    <div class=\"_title\">CONTACTEZ NOUS</div>
                    <ul class=\"list-unstyled\">
                        <li>
                            <span class=\"text-gray\"> <b style=\"color: white;\" class=\"glyphicon glyphicon-map-marker\"></b> <strong>Direction Générale, </strong> BP : 8200 Bali Douala Cameroun </span>
                        </li>
                        <li>
                            <span class=\"text-gray\"> <b style=\"color: white;\" class=\"glyphicon glyphicon-phone\"></b> (237) 233 41 64 75 | (237) 691 100 592</span>
                        </li>
                        <li>
                            <span class=\"text-gray\"><b style=\"color: white;\" class=\"glyphicon glyphicon-time\"></b> LUNDI - SAMEDI : 08.00 - 18.00</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">
                <div class=\"subfooter\">
                    Copyright &copy 2017 CDS Technologies Sarl. Réalisé par <a href=\"http://www.3mesoft.com\">3mesoft</a>
                </div>
            </div>
        </div>
    </div>
</footer>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "BlogArticleBundle:Tool:footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 46,  101 => 44,  95 => 41,  90 => 40,  88 => 39,  81 => 35,  73 => 30,  65 => 25,  57 => 20,  49 => 15,  33 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<footer class=\"footer-black footer\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"footer-body\">
                <div class=\"col-lg-4 col-md-4 col-sm-3 col-xs-12\">
                    <div class=\"_title\">A PROPOS</div>
                    <p>
                        <span class=\"main\">CDS TECHNOLOGIES Sarl</span> est une entreprise de droit Camerounais à vocation sous régionale dont la création résulte d'une réelle volonté de ses fondateurs, de disposer dans le domaine  des télécommunications au Cameroun.
                    </p>                
                </div>
                <div class=\"col-lg-4 col-md-4 col-sm-3 col-xs-12\">
                    <div class=\"_title\">LIENS UTILES</div>
                    <ul class=\"list-unstyled\">
                        <li >
                            <a href=\"{{ path(\"home\")}}\">
                                > Accueil
                            </a>
                        </li>
                        <li >
                            <a  href=\"{{ path(\"service\")}}\">
                                > Services
                            </a>
                        </li>
                        <li >
                            <a  href=\"{{ path(\"produit\")}}\">
                                > Produits
                            </a>
                        </li>
                        <li >
                            <a  href=\"{{ path(\"ed_blog_homepage\")}}\">
                                > Actualités
                            </a>
                        </li>
                        <li >
                            <a  href=\"{{ path(\"contact\")}}\">
                                > Contact
                            </a>
                        </li>
                        {% if is_granted('ROLE_BLOG_USER') %}
                            <li ><a href=\"{{ path('sonata_admin_dashboard') }}\">  > Administration Generale</a></li>
                            <li ><a href=\"{{ path('fos_user_security_logout') }}\">  > Deconnexion</a></li>

                        {% else %}
                            <li ><a href=\"{{ path('fos_user_security_login') }}\">  > Connexion</a></li>
                            {% endif %}
                        
                    </ul>
                </div>
                <div class=\"col-lg-4 col-md-4 col-sm-3 col-xs-12\">
                    <div class=\"_title\">CONTACTEZ NOUS</div>
                    <ul class=\"list-unstyled\">
                        <li>
                            <span class=\"text-gray\"> <b style=\"color: white;\" class=\"glyphicon glyphicon-map-marker\"></b> <strong>Direction Générale, </strong> BP : 8200 Bali Douala Cameroun </span>
                        </li>
                        <li>
                            <span class=\"text-gray\"> <b style=\"color: white;\" class=\"glyphicon glyphicon-phone\"></b> (237) 233 41 64 75 | (237) 691 100 592</span>
                        </li>
                        <li>
                            <span class=\"text-gray\"><b style=\"color: white;\" class=\"glyphicon glyphicon-time\"></b> LUNDI - SAMEDI : 08.00 - 18.00</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">
                <div class=\"subfooter\">
                    Copyright &copy 2017 CDS Technologies Sarl. Réalisé par <a href=\"http://www.3mesoft.com\">3mesoft</a>
                </div>
            </div>
        </div>
    </div>
</footer>
{#
<div class=\"bg-blue marginTop-lg footer-bar hidden-xs\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-9 col-md-9 col-sm-9 col-xs-12\">
                <span class=\"text-white\">VOUS SATISFAIRE, NOTRE PRIORITE</span>
            </div>
            <div class=\"col-lg-4 col-md-4 col-sm-3 col-xs-12\">
                <a href=\"{{ path(\"contact\")}}\" class=\"btn btn-white\">
                    NOUS CONTACTER
                </a>
            </div>
        </div>
    </div>
</div>


<footer>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-4 col-md-4 col-sm-12 col-xs-12\">
                <div class=\"title\">
                    <img src=\"{{ asset(\"assets/img/logo.png\") }}\" alt=\"\" class=\"img-responsive\" style=\"
    background: white;
    padding: 10px;
    border-radius: 10px;
\">
                </div>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro illo labore excepturi, ab, dolorem placeat. Voluptatem, nostrum, delectus, excepturi eius perspiciatis minima qui et voluptas enim praesentium velit quas fuga.
                </p>
            </div>
            <div class=\"col-lg-4 col-md-4 col-sm-12 col-xs-12\">
                <div class=\"text-blue title\">Plan du site</div>
                <ul class=\"list-unstyled\">
                    <li >
                        <a href=\"{{ path(\"home\")}}\">
                            Accueil
                        </a>
                    </li>
                    <li >
                        <a  href=\"{{ path(\"service\")}}\">
                            Services
                        </a>
                    </li>
                    <li >
                        <a  href=\"{{ path(\"produit\")}}\">
                            Produits
                        </a>
                    </li>
                    <li >
                        <a  href=\"{{ path(\"ed_blog_homepage\")}}\">
                            Actualités
                        </a>
                    </li>
                    <li >
                        <a  href=\"{{ path(\"contact\")}}\">
                            Contacts
                        </a>
                    </li>
                </ul>
            </div>
            <div class=\"col-lg-4 col-md-4 col-sm-12 col-xs-12\">
                <div class=\"text-blue title\">CONTACT</div>
                <ul class=\"list-unstyled\">
                    <li>
                        BP xxxx DLA CAMEROUN
                    </li>
                    <li>
                        <i class=\"fa fa-phone\"></i> 237 xxx xxx xxx
                    </li>
                    <li>
                        <i class=\"fa fa-envelope\"></i> contact@xxx.xxx
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id=\"copyright\" class=\"col-xs-12 text-right\">
        <div class=\"container\">
            © Copyright {{ \"now\"|date(\"Y\") }} powered By <a target=\"_blank\" href=\"http://www.3mesoft.com\">3mesoft.com</a>
        </div>
    </div>
</footer>
#}", "BlogArticleBundle:Tool:footer.html.twig", "C:\\wamp64\\www\\cds\\src\\Blog\\ArticleBundle/Resources/views/Tool/footer.html.twig");
    }
}
