<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* ::base.html.twig */
class __TwigTemplate_03f6369aaaf4a8ca236cf77bf0b06c4eb5faf725688d41a33005710859d5fac5 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'meta' => [$this, 'block_meta'],
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'header' => [$this, 'block_header'],
            'body' => [$this, 'block_body'],
            'footer' => [$this, 'block_footer'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
";
        // line 2
        ob_start();
        // line 3
        echo "    <html xmlns:og=\"http://ogp.me/ns#\" xmlns:fb=\"http://www.facebook.com/2008/fbml\"  xmlns:og=\"http://opengraphprotocol.org/schema/\" lang=\"fr\" xml:lang=\"fr\">
        <head>
            <meta charset=\"UTF-8\" />        
            <meta name=\"keywords\" content=\"Cameroun, CDS Technologie SARL, Fibre optique,Cameroon.\">
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />        
            <meta property=\"og:type\" content=\"website\">       
            <meta property=\"og:site_name\" content=\"www.cdstechnologie.com\">
            <meta name=\"viewport\" content=\" initial-scale=1.0\">
            <meta name=\"description\" content=\"CDS TECHNOLOGIES Sarl est une entreprise de droit Camerounais à vocation sous régionale. Dont la création résulte d'une réelle volonté de ses fondateurs, de disposer dans le domaine des télécommunications au Cameroun, d’une grande entreprise pouvant offrir aux usagers de meilleurs prestations, une gamme variée de produits et de services innovants.\">
            ";
        // line 12
        $this->displayBlock('meta', $context, $blocks);
        // line 18
        echo "
            <title>";
        // line 19
        $this->displayBlock('title', $context, $blocks);
        echo " - CDS TECHNOLOGIE SARL</title>
            <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Roboto\">
            <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:300,400\" rel=\"stylesheet\">
            ";
        // line 22
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "5676372_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_5676372_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/5676372_bootstrap_1.css");
            // line 23
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\">
            ";
            // asset "5676372_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_5676372_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/5676372_fa_2.css");
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\">
            ";
            // asset "5676372_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_5676372_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/5676372_header_3.css");
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\">
            ";
            // asset "5676372_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_5676372_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/5676372_main_4.css");
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\">
            ";
        } else {
            // asset "5676372"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_5676372") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/5676372.css");
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\">
            ";
        }
        unset($context["asset_url"]);
        // line 25
        echo "            ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "7180c87_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_7180c87_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/compiled_7180c87_jquery-2.1.0.min_1.js");
            echo "  
            <script src=\"";
            // line 26
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script> ";
            // asset "7180c87_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_7180c87_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/compiled_7180c87_router_2.js");
            // line 25
            echo "  
            <script src=\"";
            // line 26
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script> ";
            // asset "7180c87_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_7180c87_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/compiled_7180c87_loadingRoute_3.js");
            // line 25
            echo "  
            <script src=\"";
            // line 26
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script> ";
        } else {
            // asset "7180c87"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_7180c87") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/compiled_7180c87.js");
            // line 25
            echo "  
            <script src=\"";
            // line 26
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script> ";
        }
        unset($context["asset_url"]);
        // line 27
        echo "            <script src=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_js_routing_js", ["callback" => "fos.Router.setData"]);
        echo "\"></script>

        ";
        // line 29
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 30
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/favicon.png"), "html", null, true);
        echo "\" />
    </head>
    <body style=\"padding: 0px;\">


        ";
        // line 35
        $this->displayBlock('header', $context, $blocks);
        // line 38
        echo "        ";
        $this->displayBlock('body', $context, $blocks);
        // line 40
        echo "
        ";
        // line 41
        $this->displayBlock('footer', $context, $blocks);
        // line 42
        echo "
    </div>
    ";
        // line 44
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3f914b1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3f914b1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/compiled_3f914b1_scrollReveal.min_1.js");
            echo "  
    <script src=\"";
            // line 45
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script> ";
            // asset "3f914b1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3f914b1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/compiled_3f914b1_bootstrap.min_2.js");
            // line 44
            echo "  
    <script src=\"";
            // line 45
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script> ";
            // asset "3f914b1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3f914b1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/compiled_3f914b1_classie_3.js");
            // line 44
            echo "  
    <script src=\"";
            // line 45
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script> ";
        } else {
            // asset "3f914b1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3f914b1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/compiled_3f914b1.js");
            // line 44
            echo "  
    <script src=\"";
            // line 45
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script> ";
        }
        unset($context["asset_url"]);
        // line 46
        echo "    <script>
        function init() {
            window.addEventListener('scroll', function(e) {
                var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                        shrinkOn = 250,
                        header = document.getElementById(\"header\");
                if (distanceY > shrinkOn) {
                    classie.add(header, \"smaller\");
                } else {
                    if (classie.has(header, \"smaller\")) {
                        classie.remove(header, \"smaller\");
                    }
                }
            });
        }
        window.onload = init();
        \$(document).ready(function() {
            \$(\"#array\").click(function() {
                \$(\"body\").animate({scrollTop: \$(window).height()- 60}, 1500);
            })
        })

    </script>
    ";
        // line 69
        $this->displayBlock('javascripts', $context, $blocks);
        // line 72
        echo "


</body>
</html>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 12
    public function block_meta($context, array $blocks = [])
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "meta"));

        // line 13
        echo "                <meta property=\"og:url\" content=\"http://www.cdstechnologie.com/\">
                <meta property=\"og:image\" content=\"http://www.cdstechnologie.com/";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/logo.gif"), "html", null, true);
        echo " \">
                <meta property=\"og:title\" content=\"CDS TECHNOLOGIE SARL\">
                <meta property=\"og:description\" content=\"CDS TECHNOLOGIES Sarl est une entreprise de droit Camerounais à vocation sous régionale. Dont la création résulte d'une réelle volonté de ses fondateurs, de disposer dans le domaine des télécommunications au Cameroun, d’une grande entreprise pouvant offrir aux usagers de meilleurs prestations, une gamme variée de produits et de services innovants.\" />
            ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 19
    public function block_title($context, array $blocks = [])
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo " Accueil ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 29
    public function block_stylesheets($context, array $blocks = [])
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 35
    public function block_header($context, array $blocks = [])
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        echo "        
            ";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->controller("BlogArticleBundle:Tool:header"));
        echo "
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 38
    public function block_body($context, array $blocks = [])
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 39
        echo "        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 41
    public function block_footer($context, array $blocks = [])
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "footer"));

        echo " ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->controller("BlogArticleBundle:Tool:footer"));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 69
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 70
        echo "
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  339 => 70,  333 => 69,  320 => 41,  313 => 39,  307 => 38,  298 => 36,  290 => 35,  279 => 29,  267 => 19,  256 => 14,  253 => 13,  247 => 12,  234 => 72,  232 => 69,  207 => 46,  202 => 45,  199 => 44,  193 => 45,  190 => 44,  185 => 45,  182 => 44,  177 => 45,  171 => 44,  167 => 42,  165 => 41,  162 => 40,  159 => 38,  157 => 35,  148 => 30,  146 => 29,  140 => 27,  135 => 26,  132 => 25,  126 => 26,  123 => 25,  118 => 26,  115 => 25,  110 => 26,  103 => 25,  71 => 23,  67 => 22,  61 => 19,  58 => 18,  56 => 12,  45 => 3,  43 => 2,  40 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
{% spaceless %}
    <html xmlns:og=\"http://ogp.me/ns#\" xmlns:fb=\"http://www.facebook.com/2008/fbml\"  xmlns:og=\"http://opengraphprotocol.org/schema/\" lang=\"fr\" xml:lang=\"fr\">
        <head>
            <meta charset=\"UTF-8\" />        
            <meta name=\"keywords\" content=\"Cameroun, CDS Technologie SARL, Fibre optique,Cameroon.\">
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />        
            <meta property=\"og:type\" content=\"website\">       
            <meta property=\"og:site_name\" content=\"www.cdstechnologie.com\">
            <meta name=\"viewport\" content=\" initial-scale=1.0\">
            <meta name=\"description\" content=\"CDS TECHNOLOGIES Sarl est une entreprise de droit Camerounais à vocation sous régionale. Dont la création résulte d'une réelle volonté de ses fondateurs, de disposer dans le domaine des télécommunications au Cameroun, d’une grande entreprise pouvant offrir aux usagers de meilleurs prestations, une gamme variée de produits et de services innovants.\">
            {% block meta %}
                <meta property=\"og:url\" content=\"http://www.cdstechnologie.com/\">
                <meta property=\"og:image\" content=\"http://www.cdstechnologie.com/{{ asset('img/logo.gif') }} \">
                <meta property=\"og:title\" content=\"CDS TECHNOLOGIE SARL\">
                <meta property=\"og:description\" content=\"CDS TECHNOLOGIES Sarl est une entreprise de droit Camerounais à vocation sous régionale. Dont la création résulte d'une réelle volonté de ses fondateurs, de disposer dans le domaine des télécommunications au Cameroun, d’une grande entreprise pouvant offrir aux usagers de meilleurs prestations, une gamme variée de produits et de services innovants.\" />
            {% endblock %}

            <title>{% block title %} Accueil {% endblock %} - CDS TECHNOLOGIE SARL</title>
            <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Roboto\">
            <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:300,400\" rel=\"stylesheet\">
            {% stylesheets filter='uglifycss'  \"assets/bootstrap/css/bootstrap.css\" \"assets/fa/css/fa.css\" '@BlogArticleBundle/Resources/public/css/header.css' \"assets/main/css/main.css\"   %}
            <link rel=\"stylesheet\" href=\"{{ asset_url }}\">
            {% endstylesheets %}
            {% javascripts filter=\"uglifyjs2\" output='js/compiled_*.js'  \"@BlogArticleBundle/Resources/public/js/jquery-2.1.0.min.js\" 'bundles/fosjsrouting/js/router.js' '@BlogArticleBundle/Resources/public/js/loadingRoute.js' %}  
            <script src=\"{{ asset_url }}\"></script> {% endjavascripts %}
            <script src=\"{{ path('fos_js_routing_js', {'callback': 'fos.Router.setData'}) }}\"></script>

        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('img/favicon.png') }}\" />
    </head>
    <body style=\"padding: 0px;\">


        {% block header %}        
            {{render(controller('BlogArticleBundle:Tool:header')) }}
        {% endblock %}
        {% block body %}
        {% endblock %}

        {% block footer %} {{render(controller('BlogArticleBundle:Tool:footer')) }}{% endblock %}

    </div>
    {% javascripts filter=\"uglifyjs2\" output='js/compiled_*.js'   \"@BlogArticleBundle/Resources/public/js/scrollReveal.min.js\"     '@BlogArticleBundle/Resources/public/js/bootstrap.min.js' \"@BlogArticleBundle/Resources/public/js/classie.js\"   %}  
    <script src=\"{{ asset_url }}\"></script> {% endjavascripts %}
    <script>
        function init() {
            window.addEventListener('scroll', function(e) {
                var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                        shrinkOn = 250,
                        header = document.getElementById(\"header\");
                if (distanceY > shrinkOn) {
                    classie.add(header, \"smaller\");
                } else {
                    if (classie.has(header, \"smaller\")) {
                        classie.remove(header, \"smaller\");
                    }
                }
            });
        }
        window.onload = init();
        \$(document).ready(function() {
            \$(\"#array\").click(function() {
                \$(\"body\").animate({scrollTop: \$(window).height()- 60}, 1500);
            })
        })

    </script>
    {% block javascripts %}

    {% endblock %}



</body>
</html>
{% endspaceless %}", "::base.html.twig", "C:\\wamp64\\www\\cds\\app/Resources\\views/base.html.twig");
    }
}
