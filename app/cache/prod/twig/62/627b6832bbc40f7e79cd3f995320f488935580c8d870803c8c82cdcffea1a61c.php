<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* BlogArticleBundle:Public:index.html.twig */
class __TwigTemplate_13b8b88b7b9d4d2372d2edc93e2a42b07c69050dc7dff894f3ac2a14952a1398 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "BlogArticleBundle:Public:index.html.twig"));

        $this->parent = $this->loadTemplate("::base.html.twig", "BlogArticleBundle:Public:index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_stylesheets($context, array $blocks = [])
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 5
        echo "    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "c7974be_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_c7974be_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/c7974be_style_common_1.css");
            // line 6
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\">
    ";
            // asset "c7974be_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_c7974be_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/c7974be_style10_2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\">
    ";
            // asset "c7974be_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_c7974be_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/c7974be_owl.carousel.min_3.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\">
    ";
            // asset "c7974be_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_c7974be_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/c7974be_owl.theme.default.min_4.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\">
    ";
        } else {
            // asset "c7974be"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_c7974be") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/c7974be.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\">
    ";
        }
        unset($context["asset_url"]);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 10
    public function block_body($context, array $blocks = [])
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 11
        echo "    <style>
        section.module.parallax-2 {
               background: #0d67ac;
            margin-top: 80px;
            padding-top: 9%
        }    
    </style>
    <div id=\"myCarousel\" class=\"carousel slide\" data-ride=\"carousel\">
        <div class=\"carousel-inner\" role=\"listbox\">
            ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["slides"] ?? $this->getContext($context, "slides")));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["slide"]) {
            // line 21
            echo "                <div   class=\"item ";
            if (($this->getAttribute($context["loop"], "index", []) == 1)) {
                echo " active ";
            }
            echo "\">
                    <img class=\"first-slide\" src=\"";
            // line 22
            echo $this->env->getExtension('sonata_media')->path($this->getAttribute($context["slide"], "image", []), "reference");
            echo "\" alt=\"Slide";
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", []), "html", null, true);
            echo "\">
                    <div class=\"container\">
                        <div class=\"carousel-caption hidden-xs\">
                            <h1> ";
            // line 25
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($context["slide"], "name", []), 0, 53), "html", null, true);
            echo "</h1>
                            ";
            // line 26
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute($context["slide"], "descr", []), 0, 436), "html", null, true);
            echo "<br>
                           ";
            // line 27
            if ($this->getAttribute($context["slide"], "lien", [])) {
                echo " <a href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["slide"], "lien", []), "html", null, true);
                echo "\" class=\"btn btn-default\">En savoir plus</a> ";
            }
            // line 28
            echo "                        </div>
                    </div>
                </div>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['slide'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "
        </div>
        <a class=\"left carousel-control\" href=\"#myCarousel\" role=\"button\" data-slide=\"prev\">
            <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>
            <span class=\"sr-only\">PrÃ©cÃ©dent</span>
        </a>
        <a class=\"right carousel-control\" href=\"#myCarousel\" role=\"button\" data-slide=\"next\">
            <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>
            <span class=\"sr-only\">Suivant</span>
        </a>
    </div><!-- /.carousel -->
    <div class=\"marginTop-lg\">
        <div class=\"container\">
            <svg id=\"array\" class=\"arrows\">
            <path class=\"a1\" d=\"M0 0 L30 32 L60 0\"></path>
            <path class=\"a2\" d=\"M0 20 L30 52 L60 20\"></path>
            <path class=\"a3\" d=\"M0 40 L30 72 L60 40\"></path>
            </svg>
        </div>
    </div>
    <!-- servicess -->
    <div class=\"marginTop-lg block\">
        <div class=\"container\">
            <div class=\"row margin-md\">
                <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">
                    <div class=\"text-center\">
                        <h2 id=\"service\"> Nos <strong>Services</strong> </h2>
                    </div>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"services col-sm-6 col-sm-12 col-xs-12\"  data-sr=\"enter top, move 10px\">
                    <div class=\"service-img-container\">
                        <div> <img src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/service1.jpg"), "html", null, true);
        echo "\" class=\"img-responsive w100\" alt=\"\"></div>
                    </div>
                    <h3>
                        <a href=\"";
        // line 68
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("service");
        echo "#ro\" class=\"text-blue\">
                            Réseaux Optiques
                        </a>
                    </h3>
                    <div>
                        <p>
                            Notre équipe étant principalement spécialisé dans les réseaux par fibre optique, nous proposons des services de bout en bout qui constituent un réseau optique de la phase de conception à la réalisation en tenant compte des spécifications du client. 
                        </p>
                        <a class=\"pull-right text-blue\" href=\"";
        // line 76
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("service");
        echo "#rs\">En savoir plus ></a>
                    </div>
                </div>
                <div class=\" services col-sm-6 col-sm-12 col-xs-12\" data-sr=\"enter top, move 10px\">
                    <div class=\"service-img-container\">
                        <div> <img src=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/service2.jpg"), "html", null, true);
        echo "\" class=\"img-responsive w100\" alt=\"\"> </div>
                    </div>
                    <h3>
                        <a href=\"";
        // line 84
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("service");
        echo "#ri\" class=\"text-blue\">
                            Réseaux informatiques
                        </a>
                    </h3>
                    <div>
                        <p>
                       Notre département spécialisé dans la réalisation la conception, l’optimisation, la sécurité et la maintenance  des systèmes informatiques et réseaux est à votre disposition pour vous proposer des solutions sur mesure, dans le respect des normes internationales des architectures.
                        </p>
                        <a class=\"pull-right text-blue\" href=\"#\">En savoir plus ></a>
                    </div>
                </div>
                <div class=\"services col-md-4 col-sm-6 col-sm-12 col-xs-12\"  data-sr=\"enter top, move 10px\">
                    <div class=\"service-img-container\">
                        <div> <img src=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/service3.jpg"), "html", null, true);
        echo "\" class=\"img-responsive h100\" alt=\"\"> </div>
                    </div>
                    <h3>
                        <a href=\"";
        // line 100
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("service");
        echo "#sca\" class=\"text-blue\">
                       Sécurité et contrôle d’accès 
                        </a>
                    </h3>
                    <div>
                        <p>
                        Nos équipes grace leurs compétances dans la sécurité, mettent sur pieds des solutions mesures pour assurrer l’intégrité de vos infrastructures et ressources
                        </p>
                        <a class=\"pull-right text-blue\" href=\"";
        // line 108
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("service");
        echo "#sca\">En savoir plus ></a>
                    </div>
                </div>
                <div class=\"services col-md-4 col-sm-6 col-xs-12\"  data-sr=\"enter top, move 10px\">
                    <div class=\"service-img-container\">
                        <div> <img src=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/service4.jpg"), "html", null, true);
        echo "\" class=\"img-responsive h100\" alt=\"\"> </div>
                    </div>
                    <h3>
                        <a href=\"";
        // line 116
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("service");
        echo "#ener\" class=\"text-blue\">
                           Energie
                        </a>
                    </h3>
                    <div>
                        <p>
                            Type de construction, situation, besoins de chacun en énergie... 
                            Nous calculons et simulons la performance globale de votre système énergétique et sélectionnons soigneusement les composants
                            pour vous fournir une solution sur mesure.
                        </p>
                        <a class=\"pull-right text-blue\" href=\"";
        // line 126
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("service");
        echo "#energie\">En savoir plus ></a>
                    </div>
                </div>
                <div class=\" services col-md-4 col-sm-12 col-xs-12\"  data-sr=\"enter top, move 10px\">
                    <div class=\"service-img-container\">
                        <div> <img src=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/service5.jpg"), "html", null, true);
        echo "\" class=\"img-responsive h100\" alt=\"\"> </div>
                    </div>
                    <h3>
                        <a href=\"";
        // line 134
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("service");
        echo "#rm\" class=\"text-blue\">
                           Réseaux mobiles : CDS Technologies
                        </a>
                    </h3>
                    <div>
                        <p>
                        a acquis et acquiert peu à peu une solide expérience dans ce domaine. Canopy, Shelter, local technique de sites télécoms, ... font partie de notre jargon.   
                        </p>
                        <a class=\"pull-right text-blue\" href=\"";
        // line 142
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("service");
        echo "#rm\">En savoir plus ></a>
                    </div>
                </div>

            </div>
            <div class=\"row marginTop-lg\">
                <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">
                    <div class=\"text-center\">
                        <a href=\"";
        // line 150
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("service");
        echo "\" class=\"btn btn-blue\">
                            TOUS LES SERVICES
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    ";
        // line 161
        echo "
    <!-- produits -->
    <section class=\"block module parallax parallax-2\">
        <div class=\"container\">
            <h1>CDS TECHNOLOGIES SARL<br><small data-sr=\"enter bottom, move 100px,reset\"> The innovation, our priority</small></h1>
        </div>
    </section>

    ";
        // line 169
        if (twig_length_filter($this->env, ($context["categories"] ?? $this->getContext($context, "categories")))) {
            // line 170
            echo "        <!-- rÃ©fÃ©rences -->
        <div class=\"marginTop-lg block\">
            <div class=\"container\">
                <div class=\"row margin-md\">
                    <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">
                        <div class=\"text-center\">
                            <h2> Nos <strong>Produits</strong> </h2>
                        </div>
                    </div>
                </div>
                <div class=\"row\">
                    ";
            // line 181
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? $this->getContext($context, "categories")));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 182
                echo "                        <div class=\"view view-tenth\">
                            <img src=\"";
                // line 183
                echo $this->env->getExtension('sonata_media')->path($this->getAttribute($context["category"], "image", []), "lib");
                echo "\" />
                            <div class=\"mask\">
                                <h2>";
                // line 185
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "name", []), "html", null, true);
                echo "</h2>
                                <p>";
                // line 186
                echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "descr", []), "html", null, true);
                echo "</p>
                                <a href=\"";
                // line 187
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("product", ["slug" => $this->getAttribute($context["category"], "slug", [])]), "html", null, true);
                echo "\" class=\"info\">En savoir plus</a>
                            </div>
                        </div>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 191
            echo "                </div>
                <div class=\"row marginTop-lg\">
                    <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">
                        <div class=\"text-center\">
                            <a href=\"";
            // line 195
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("produit");
            echo "\" class=\"btn btn-blue\">
                                TOUTES LES CATEGORIES
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- rÃ©fÃ©rences -->
    ";
        }
        // line 205
        echo "    <div class=\"marginTop-lg padding-lg grey\">
        <div class=\"container\">
            <div class=\"row margin-md\">
                <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">
                    <div class=\"text-center\">
                        <h2> NOS PARTENAIRES </h2>
                    </div>
                </div>
            </div>

            <div class=\"row\">
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"";
        // line 217
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/nextel.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                    <img class=\"nope\" src=\"";
        // line 218
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/nextel_black.png"), "html", null, true);
        echo "\"  width=\"100px\"> 
                </div>
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"";
        // line 221
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/cegelec.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                    <img class=\"nope\" src=\"";
        // line 222
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/cegelec_black.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                </div>
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"";
        // line 225
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/cotco.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                    <img class=\"nope\" src=\"";
        // line 226
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/cotco_black.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                </div>

                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"";
        // line 230
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/camusat.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                    <img class=\"nope\" src=\"";
        // line 231
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/camusat_black.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                </div>
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"";
        // line 234
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/camrail.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                    <img class=\"nope\" src=\"";
        // line 235
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/camrail_black.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                </div>
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"";
        // line 238
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/orange.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                    <img class=\"nope\" src=\"";
        // line 239
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/orange_black.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                </div>
            </div>
                <div class=\"row\">
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"";
        // line 244
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/eneo.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                    <img class=\"nope\" src=\"";
        // line 245
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/eneo_black.png"), "html", null, true);
        echo "\"  width=\"100px\"> 
                </div>
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"";
        // line 248
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/art.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                    <img class=\"nope\" src=\"";
        // line 249
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/art_black.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                </div>
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"";
        // line 252
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/scg.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                    <img class=\"nope\" src=\"";
        // line 253
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/scg_black.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                </div>

                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"";
        // line 257
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/php.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                    <img class=\"nope\" src=\"";
        // line 258
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/php_black.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                </div>
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"";
        // line 261
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/minpostel.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                    <img class=\"nope\" src=\"";
        // line 262
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/minpostel_black.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                </div>
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"";
        // line 265
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/iuc.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                    <img class=\"nope\" src=\"";
        // line 266
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("img/iuc_black.png"), "html", null, true);
        echo "\" width=\"100px\"> 
                </div>
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 273
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 274
        echo "    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "5d8e403_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_5d8e403_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/compiled_5d8e403_owl.carousel.min_1.js");
            echo "  
    <script src=\"";
            // line 275
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script> ";
            // asset "5d8e403_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_5d8e403_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/compiled_5d8e403_jquery.cbslideheader.min_2.js");
            // line 274
            echo "  
    <script src=\"";
            // line 275
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script> ";
        } else {
            // asset "5d8e403"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_5d8e403") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/compiled_5d8e403.js");
            // line 274
            echo "  
    <script src=\"";
            // line 275
            echo twig_escape_filter($this->env, ($context["asset_url"] ?? $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script> ";
        }
        unset($context["asset_url"]);
        // line 276
        echo "
    <script>

        \$(document).ready(function() {
            
            window.scrollReveal = new scrollReveal();
            \$(\"#produit\").owlCarousel({
                autoplay: true,
                autoplayTimeout: 3000,
                loop: true,
                navigation: false,
                navigationText: [\"<span class='glyphicon glyphicon-chevron-left'></span>\", \"<span class='glyphicon glyphicon-chevron-right'></span>\"],
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 2,
                    },
                    396: {
                        items: 2,
                    },
                    580: {
                        items: 2,
                    },
                    766: {
                        items: 3,
                    },
                    800: {
                        items: 2,
                    },
                    1000: {
                        items: 3,
                    }
                }
            });
        })

    </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "BlogArticleBundle:Public:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  604 => 276,  599 => 275,  596 => 274,  590 => 275,  587 => 274,  582 => 275,  575 => 274,  569 => 273,  556 => 266,  552 => 265,  546 => 262,  542 => 261,  536 => 258,  532 => 257,  525 => 253,  521 => 252,  515 => 249,  511 => 248,  505 => 245,  501 => 244,  493 => 239,  489 => 238,  483 => 235,  479 => 234,  473 => 231,  469 => 230,  462 => 226,  458 => 225,  452 => 222,  448 => 221,  442 => 218,  438 => 217,  424 => 205,  411 => 195,  405 => 191,  395 => 187,  391 => 186,  387 => 185,  382 => 183,  379 => 182,  375 => 181,  362 => 170,  360 => 169,  350 => 161,  337 => 150,  326 => 142,  315 => 134,  309 => 131,  301 => 126,  288 => 116,  282 => 113,  274 => 108,  263 => 100,  257 => 97,  241 => 84,  235 => 81,  227 => 76,  216 => 68,  210 => 65,  175 => 32,  158 => 28,  152 => 27,  148 => 26,  144 => 25,  136 => 22,  129 => 21,  112 => 20,  101 => 11,  95 => 10,  58 => 6,  53 => 5,  47 => 4,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends '::base.html.twig' %}


{% block stylesheets %}
    {% stylesheets filter='uglifycss' \"@BlogArticleBundle/Resources/public/css/style_common.css\"  \"@BlogArticleBundle/Resources/public/css/style10.css\" \"@BlogArticleBundle/Resources/public/css/owl.carousel.min.css\" \"@BlogArticleBundle/Resources/public/css/owl.theme.default.min.css\"  %}
    <link rel=\"stylesheet\" href=\"{{ asset_url }}\">
    {% endstylesheets %}
{% endblock %}

{% block body %}
    <style>
        section.module.parallax-2 {
               background: #0d67ac;
            margin-top: 80px;
            padding-top: 9%
        }    
    </style>
    <div id=\"myCarousel\" class=\"carousel slide\" data-ride=\"carousel\">
        <div class=\"carousel-inner\" role=\"listbox\">
            {% for slide in slides %}
                <div   class=\"item {% if loop.index==1 %} active {% endif %}\">
                    <img class=\"first-slide\" src=\"{% path slide.image,\"reference\" %}\" alt=\"Slide{{ loop.index }}\">
                    <div class=\"container\">
                        <div class=\"carousel-caption hidden-xs\">
                            <h1> {{ slide.name|slice(0,53) }}</h1>
                            {{ slide.descr|slice(0, 436) }}<br>
                           {% if slide.lien %} <a href=\"{{ slide.lien}}\" class=\"btn btn-default\">En savoir plus</a> {% endif %}
                        </div>
                    </div>
                </div>
            {% endfor %}

        </div>
        <a class=\"left carousel-control\" href=\"#myCarousel\" role=\"button\" data-slide=\"prev\">
            <span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>
            <span class=\"sr-only\">PrÃ©cÃ©dent</span>
        </a>
        <a class=\"right carousel-control\" href=\"#myCarousel\" role=\"button\" data-slide=\"next\">
            <span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>
            <span class=\"sr-only\">Suivant</span>
        </a>
    </div><!-- /.carousel -->
    <div class=\"marginTop-lg\">
        <div class=\"container\">
            <svg id=\"array\" class=\"arrows\">
            <path class=\"a1\" d=\"M0 0 L30 32 L60 0\"></path>
            <path class=\"a2\" d=\"M0 20 L30 52 L60 20\"></path>
            <path class=\"a3\" d=\"M0 40 L30 72 L60 40\"></path>
            </svg>
        </div>
    </div>
    <!-- servicess -->
    <div class=\"marginTop-lg block\">
        <div class=\"container\">
            <div class=\"row margin-md\">
                <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">
                    <div class=\"text-center\">
                        <h2 id=\"service\"> Nos <strong>Services</strong> </h2>
                    </div>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"services col-sm-6 col-sm-12 col-xs-12\"  data-sr=\"enter top, move 10px\">
                    <div class=\"service-img-container\">
                        <div> <img src=\"{{ asset(\"img/service1.jpg\") }}\" class=\"img-responsive w100\" alt=\"\"></div>
                    </div>
                    <h3>
                        <a href=\"{{ path(\"service\")}}#ro\" class=\"text-blue\">
                            Réseaux Optiques
                        </a>
                    </h3>
                    <div>
                        <p>
                            Notre équipe étant principalement spécialisé dans les réseaux par fibre optique, nous proposons des services de bout en bout qui constituent un réseau optique de la phase de conception à la réalisation en tenant compte des spécifications du client. 
                        </p>
                        <a class=\"pull-right text-blue\" href=\"{{ path(\"service\")}}#rs\">En savoir plus ></a>
                    </div>
                </div>
                <div class=\" services col-sm-6 col-sm-12 col-xs-12\" data-sr=\"enter top, move 10px\">
                    <div class=\"service-img-container\">
                        <div> <img src=\"{{ asset(\"img/service2.jpg\") }}\" class=\"img-responsive w100\" alt=\"\"> </div>
                    </div>
                    <h3>
                        <a href=\"{{ path(\"service\")}}#ri\" class=\"text-blue\">
                            Réseaux informatiques
                        </a>
                    </h3>
                    <div>
                        <p>
                       Notre département spécialisé dans la réalisation la conception, l’optimisation, la sécurité et la maintenance  des systèmes informatiques et réseaux est à votre disposition pour vous proposer des solutions sur mesure, dans le respect des normes internationales des architectures.
                        </p>
                        <a class=\"pull-right text-blue\" href=\"#\">En savoir plus ></a>
                    </div>
                </div>
                <div class=\"services col-md-4 col-sm-6 col-sm-12 col-xs-12\"  data-sr=\"enter top, move 10px\">
                    <div class=\"service-img-container\">
                        <div> <img src=\"{{ asset(\"img/service3.jpg\") }}\" class=\"img-responsive h100\" alt=\"\"> </div>
                    </div>
                    <h3>
                        <a href=\"{{ path(\"service\")}}#sca\" class=\"text-blue\">
                       Sécurité et contrôle d’accès 
                        </a>
                    </h3>
                    <div>
                        <p>
                        Nos équipes grace leurs compétances dans la sécurité, mettent sur pieds des solutions mesures pour assurrer l’intégrité de vos infrastructures et ressources
                        </p>
                        <a class=\"pull-right text-blue\" href=\"{{ path(\"service\")}}#sca\">En savoir plus ></a>
                    </div>
                </div>
                <div class=\"services col-md-4 col-sm-6 col-xs-12\"  data-sr=\"enter top, move 10px\">
                    <div class=\"service-img-container\">
                        <div> <img src=\"{{ asset(\"img/service4.jpg\") }}\" class=\"img-responsive h100\" alt=\"\"> </div>
                    </div>
                    <h3>
                        <a href=\"{{ path(\"service\")}}#ener\" class=\"text-blue\">
                           Energie
                        </a>
                    </h3>
                    <div>
                        <p>
                            Type de construction, situation, besoins de chacun en énergie... 
                            Nous calculons et simulons la performance globale de votre système énergétique et sélectionnons soigneusement les composants
                            pour vous fournir une solution sur mesure.
                        </p>
                        <a class=\"pull-right text-blue\" href=\"{{ path(\"service\")}}#energie\">En savoir plus ></a>
                    </div>
                </div>
                <div class=\" services col-md-4 col-sm-12 col-xs-12\"  data-sr=\"enter top, move 10px\">
                    <div class=\"service-img-container\">
                        <div> <img src=\"{{ asset(\"img/service5.jpg\") }}\" class=\"img-responsive h100\" alt=\"\"> </div>
                    </div>
                    <h3>
                        <a href=\"{{ path(\"service\")}}#rm\" class=\"text-blue\">
                           Réseaux mobiles : CDS Technologies
                        </a>
                    </h3>
                    <div>
                        <p>
                        a acquis et acquiert peu à peu une solide expérience dans ce domaine. Canopy, Shelter, local technique de sites télécoms, ... font partie de notre jargon.   
                        </p>
                        <a class=\"pull-right text-blue\" href=\"{{ path(\"service\")}}#rm\">En savoir plus ></a>
                    </div>
                </div>

            </div>
            <div class=\"row marginTop-lg\">
                <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">
                    <div class=\"text-center\">
                        <a href=\"{{ path(\"service\")}}\" class=\"btn btn-blue\">
                            TOUS LES SERVICES
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {#================== fin nos servicess ==============#}

    <!-- produits -->
    <section class=\"block module parallax parallax-2\">
        <div class=\"container\">
            <h1>CDS TECHNOLOGIES SARL<br><small data-sr=\"enter bottom, move 100px,reset\"> The innovation, our priority</small></h1>
        </div>
    </section>

    {% if categories|length %}
        <!-- rÃ©fÃ©rences -->
        <div class=\"marginTop-lg block\">
            <div class=\"container\">
                <div class=\"row margin-md\">
                    <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">
                        <div class=\"text-center\">
                            <h2> Nos <strong>Produits</strong> </h2>
                        </div>
                    </div>
                </div>
                <div class=\"row\">
                    {% for category in categories %}
                        <div class=\"view view-tenth\">
                            <img src=\"{% path category.image,\"lib\" %}\" />
                            <div class=\"mask\">
                                <h2>{{ category.name }}</h2>
                                <p>{{ category.descr }}</p>
                                <a href=\"{{ path(\"product\",{\"slug\":category.slug})}}\" class=\"info\">En savoir plus</a>
                            </div>
                        </div>
                    {% endfor %}
                </div>
                <div class=\"row marginTop-lg\">
                    <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">
                        <div class=\"text-center\">
                            <a href=\"{{ path(\"produit\")}}\" class=\"btn btn-blue\">
                                TOUTES LES CATEGORIES
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- rÃ©fÃ©rences -->
    {% endif %}
    <div class=\"marginTop-lg padding-lg grey\">
        <div class=\"container\">
            <div class=\"row margin-md\">
                <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">
                    <div class=\"text-center\">
                        <h2> NOS PARTENAIRES </h2>
                    </div>
                </div>
            </div>

            <div class=\"row\">
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"{{ asset(\"img/nextel.png\")}}\" width=\"100px\"> 
                    <img class=\"nope\" src=\"{{ asset(\"img/nextel_black.png\")}}\"  width=\"100px\"> 
                </div>
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"{{ asset(\"img/cegelec.png\")}}\" width=\"100px\"> 
                    <img class=\"nope\" src=\"{{ asset(\"img/cegelec_black.png\")}}\" width=\"100px\"> 
                </div>
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"{{ asset(\"img/cotco.png\")}}\" width=\"100px\"> 
                    <img class=\"nope\" src=\"{{ asset(\"img/cotco_black.png\")}}\" width=\"100px\"> 
                </div>

                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"{{ asset(\"img/camusat.png\")}}\" width=\"100px\"> 
                    <img class=\"nope\" src=\"{{ asset(\"img/camusat_black.png\")}}\" width=\"100px\"> 
                </div>
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"{{ asset(\"img/camrail.png\")}}\" width=\"100px\"> 
                    <img class=\"nope\" src=\"{{ asset(\"img/camrail_black.png\")}}\" width=\"100px\"> 
                </div>
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"{{ asset(\"img/orange.png\")}}\" width=\"100px\"> 
                    <img class=\"nope\" src=\"{{ asset(\"img/orange_black.png\")}}\" width=\"100px\"> 
                </div>
            </div>
                <div class=\"row\">
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"{{ asset(\"img/eneo.png\")}}\" width=\"100px\"> 
                    <img class=\"nope\" src=\"{{ asset(\"img/eneo_black.png\")}}\"  width=\"100px\"> 
                </div>
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"{{ asset(\"img/art.png\")}}\" width=\"100px\"> 
                    <img class=\"nope\" src=\"{{ asset(\"img/art_black.png\")}}\" width=\"100px\"> 
                </div>
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"{{ asset(\"img/scg.png\")}}\" width=\"100px\"> 
                    <img class=\"nope\" src=\"{{ asset(\"img/scg_black.png\")}}\" width=\"100px\"> 
                </div>

                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"{{ asset(\"img/php.png\")}}\" width=\"100px\"> 
                    <img class=\"nope\" src=\"{{ asset(\"img/php_black.png\")}}\" width=\"100px\"> 
                </div>
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"{{ asset(\"img/minpostel.png\")}}\" width=\"100px\"> 
                    <img class=\"nope\" src=\"{{ asset(\"img/minpostel_black.png\")}}\" width=\"100px\"> 
                </div>
                <div class=\"col-sm-2 col-xs-6 part\"  data-sr=\"enter top, move 10px\">
                    <img src=\"{{ asset(\"img/iuc.png\")}}\" width=\"100px\"> 
                    <img class=\"nope\" src=\"{{ asset(\"img/iuc_black.png\")}}\" width=\"100px\"> 
                </div>
            </div>
        </div>
    </div>
{% endblock %} 

{% block javascripts %}
    {% javascripts filter=\"uglifyjs2\" output='js/compiled_*.js'  \"@BlogArticleBundle/Resources/public/js/owl.carousel.min.js\"  \"@BlogArticleBundle/Resources/public/js/jquery.cbslideheader.min.js\"%}  
    <script src=\"{{ asset_url }}\"></script> {% endjavascripts %}

    <script>

        \$(document).ready(function() {
            
            window.scrollReveal = new scrollReveal();
            \$(\"#produit\").owlCarousel({
                autoplay: true,
                autoplayTimeout: 3000,
                loop: true,
                navigation: false,
                navigationText: [\"<span class='glyphicon glyphicon-chevron-left'></span>\", \"<span class='glyphicon glyphicon-chevron-right'></span>\"],
                responsiveClass: true,
                responsive: {
                    0: {
                        items: 2,
                    },
                    396: {
                        items: 2,
                    },
                    580: {
                        items: 2,
                    },
                    766: {
                        items: 3,
                    },
                    800: {
                        items: 2,
                    },
                    1000: {
                        items: 3,
                    }
                }
            });
        })

    </script>
{% endblock %}
", "BlogArticleBundle:Public:index.html.twig", "C:\\wamp64\\www\\cds\\src\\Blog\\ArticleBundle/Resources/views/Public/index.html.twig");
    }
}
