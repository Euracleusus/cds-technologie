<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* BlogArticleBundle:Tool:header.html.twig */
class __TwigTemplate_6ef94b3d545cc54df595c38374f75df8e92005ba2fb1c96cc9002fea30292e2e extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "BlogArticleBundle:Tool:header.html.twig"));

        // line 1
        echo "
<nav id=\"header\" class=\"navbar navbar-fixed-top navbar-custom\">
    <div class=\"container\">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a class=\"navbar-brand\" href=\"";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("home");
        echo "\">
                <img src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/img/logo.png"), "html", null, true);
        echo "\" alt=\"Logo CDS \" class=\"img-responsive\"/>
            </a>
        </div>
        <div id=\"navbar\" class=\"navbar-collapse collapse\">                    
            <ul class=\"nav navbar-nav navbar-right\">
                <li ";
        // line 17
        if ((($context["active"] ?? $this->getContext($context, "active")) == null)) {
            echo "class=\"active\"";
        }
        echo ">
                    <a href=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("home");
        echo "\">
                        Accueil
                    </a>
                </li>
                <li ";
        // line 22
        if ((($context["active"] ?? $this->getContext($context, "active")) == "service")) {
            echo "class=\"active\"";
        }
        echo ">
                    <a  href=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("service");
        echo "\">
                        Services
                    </a>
                </li>
                <li ";
        // line 27
        if ((($context["active"] ?? $this->getContext($context, "active")) == "produit")) {
            echo "class=\"active\"";
        }
        echo ">
                    <a  href=\"";
        // line 28
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("produit");
        echo "\">
                        Produits
                    </a>
                </li>
                <li ";
        // line 32
        if ((($context["active"] ?? $this->getContext($context, "active")) == "ref")) {
            echo "class=\"active\"";
        }
        echo ">
                    <a  href=\"";
        // line 33
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ed_blog_homepage");
        echo "\">
                        Actualités
                    </a>
                </li>
                <li ";
        // line 37
        if ((($context["active"] ?? $this->getContext($context, "active")) == "apropos")) {
            echo "class=\"active\"";
        }
        echo ">
                    <a href=\"";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("apropos");
        echo "\">
                        A Propos
                    </a>
                </li>
                <li ";
        // line 42
        if ((($context["active"] ?? $this->getContext($context, "active")) == "contact")) {
            echo "class=\"active\"";
        }
        echo ">
                    <a  href=\"";
        // line 43
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("contact");
        echo "\">
                        Contact
                    </a>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</nav>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "BlogArticleBundle:Tool:header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 43,  122 => 42,  115 => 38,  109 => 37,  102 => 33,  96 => 32,  89 => 28,  83 => 27,  76 => 23,  70 => 22,  63 => 18,  57 => 17,  49 => 12,  45 => 11,  33 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("
<nav id=\"header\" class=\"navbar navbar-fixed-top navbar-custom\">
    <div class=\"container\">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a class=\"navbar-brand\" href=\"{{ path(\"home\")}}\">
                <img src=\"{{ asset(\"assets/img/logo.png\") }}\" alt=\"Logo CDS \" class=\"img-responsive\"/>
            </a>
        </div>
        <div id=\"navbar\" class=\"navbar-collapse collapse\">                    
            <ul class=\"nav navbar-nav navbar-right\">
                <li {% if active == null %}class=\"active\"{% endif %}>
                    <a href=\"{{ path(\"home\")}}\">
                        Accueil
                    </a>
                </li>
                <li {% if active == \"service\" %}class=\"active\"{% endif %}>
                    <a  href=\"{{ path(\"service\")}}\">
                        Services
                    </a>
                </li>
                <li {% if active == \"produit\" %}class=\"active\"{% endif %}>
                    <a  href=\"{{ path(\"produit\")}}\">
                        Produits
                    </a>
                </li>
                <li {% if active == \"ref\" %}class=\"active\"{% endif %}>
                    <a  href=\"{{ path(\"ed_blog_homepage\")}}\">
                        Actualités
                    </a>
                </li>
                <li {% if active == \"apropos\" %}class=\"active\"{% endif %}>
                    <a href=\"{{ path(\"apropos\")}}\">
                        A Propos
                    </a>
                </li>
                <li {% if active == \"contact\" %}class=\"active\"{% endif %}>
                    <a  href=\"{{ path(\"contact\")}}\">
                        Contact
                    </a>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</nav>

", "BlogArticleBundle:Tool:header.html.twig", "C:\\wamp64\\www\\cds\\src\\Blog\\ArticleBundle/Resources/views/Tool/header.html.twig");
    }
}
