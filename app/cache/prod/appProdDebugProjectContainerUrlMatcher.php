<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);

        if (0 === strpos($pathinfo, '/css')) {
            if (0 === strpos($pathinfo, '/css/8c7ad4a')) {
                // _assetic_8c7ad4a
                if ('/css/8c7ad4a.css' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '8c7ad4a',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_8c7ad4a',);
                }

                if (0 === strpos($pathinfo, '/css/8c7ad4a_')) {
                    // _assetic_8c7ad4a_0
                    if ('/css/8c7ad4a_header_1.css' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '8c7ad4a',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_8c7ad4a_0',);
                    }

                    // _assetic_8c7ad4a_1
                    if ('/css/8c7ad4a_right_2.css' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '8c7ad4a',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_8c7ad4a_1',);
                    }

                    // _assetic_8c7ad4a_2
                    if ('/css/8c7ad4a_index2_3.css' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '8c7ad4a',  'pos' => 2,  '_format' => 'css',  '_route' => '_assetic_8c7ad4a_2',);
                    }

                    if (0 === strpos($pathinfo, '/css/8c7ad4a_s')) {
                        // _assetic_8c7ad4a_3
                        if ('/css/8c7ad4a_slide_4.css' === $pathinfo) {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '8c7ad4a',  'pos' => 3,  '_format' => 'css',  '_route' => '_assetic_8c7ad4a_3',);
                        }

                        // _assetic_8c7ad4a_4
                        if ('/css/8c7ad4a_service_5.css' === $pathinfo) {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '8c7ad4a',  'pos' => 4,  '_format' => 'css',  '_route' => '_assetic_8c7ad4a_4',);
                        }

                    }

                    // _assetic_8c7ad4a_5
                    if ('/css/8c7ad4a_main_6.css' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '8c7ad4a',  'pos' => 5,  '_format' => 'css',  '_route' => '_assetic_8c7ad4a_5',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/css/11d9a53')) {
                // _assetic_11d9a53
                if ('/css/11d9a53.css' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '11d9a53',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_11d9a53',);
                }

                if (0 === strpos($pathinfo, '/css/11d9a53_')) {
                    // _assetic_11d9a53_0
                    if ('/css/11d9a53_pnotify.custom.min_1.css' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '11d9a53',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_11d9a53_0',);
                    }

                    // _assetic_11d9a53_1
                    if ('/css/11d9a53_jquery.Jcrop_2.css' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '11d9a53',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_11d9a53_1',);
                    }

                    // _assetic_11d9a53_2
                    if ('/css/11d9a53_nprogress_3.css' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '11d9a53',  'pos' => 2,  '_format' => 'css',  '_route' => '_assetic_11d9a53_2',);
                    }

                    // _assetic_11d9a53_3
                    if ('/css/11d9a53_bootstrap_4.css' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '11d9a53',  'pos' => 3,  '_format' => 'css',  '_route' => '_assetic_11d9a53_3',);
                    }

                    // _assetic_11d9a53_4
                    if ('/css/11d9a53_main.min.blessed_5.css' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '11d9a53',  'pos' => 4,  '_format' => 'css',  '_route' => '_assetic_11d9a53_4',);
                    }

                    // _assetic_11d9a53_5
                    if ('/css/11d9a53_bootstrap-tagsinput_6.css' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '11d9a53',  'pos' => 5,  '_format' => 'css',  '_route' => '_assetic_11d9a53_5',);
                    }

                }

            }

        }

        if (0 === strpos($pathinfo, '/js/c972cc2')) {
            // _assetic_c972cc2
            if ('/js/c972cc2.js' === $pathinfo) {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'c972cc2',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_c972cc2',);
            }

            if (0 === strpos($pathinfo, '/js/c972cc2_')) {
                // _assetic_c972cc2_0
                if ('/js/c972cc2_modernizr-2.6.2-respond-1.1.0.min_1.js' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'c972cc2',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_c972cc2_0',);
                }

                // _assetic_c972cc2_1
                if ('/js/c972cc2_retina-1.1.0.min_2.js' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'c972cc2',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_c972cc2_1',);
                }

                if (0 === strpos($pathinfo, '/js/c972cc2_i')) {
                    // _assetic_c972cc2_2
                    if ('/js/c972cc2_imagesloaded.pkgd.min_3.js' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'c972cc2',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_c972cc2_2',);
                    }

                    // _assetic_c972cc2_3
                    if ('/js/c972cc2_isotope.pkgd.min_4.js' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'c972cc2',  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_c972cc2_3',);
                    }

                }

                // _assetic_c972cc2_4
                if ('/js/c972cc2_bootstrap-tagsinput.min_5.js' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'c972cc2',  'pos' => 4,  '_format' => 'js',  '_route' => '_assetic_c972cc2_4',);
                }

                if (0 === strpos($pathinfo, '/js/c972cc2_jquery.')) {
                    // _assetic_c972cc2_5
                    if ('/js/c972cc2_jquery.autocomplete_6.js' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'c972cc2',  'pos' => 5,  '_format' => 'js',  '_route' => '_assetic_c972cc2_5',);
                    }

                    // _assetic_c972cc2_6
                    if ('/js/c972cc2_jquery.ui.widget_7.js' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'c972cc2',  'pos' => 6,  '_format' => 'js',  '_route' => '_assetic_c972cc2_6',);
                    }

                    // _assetic_c972cc2_7
                    if ('/js/c972cc2_jquery.fileupload_8.js' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => 'c972cc2',  'pos' => 7,  '_format' => 'js',  '_route' => '_assetic_c972cc2_7',);
                    }

                }

                // _assetic_c972cc2_8
                if ('/js/c972cc2_nprogress_9.js' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'c972cc2',  'pos' => 8,  '_format' => 'js',  '_route' => '_assetic_c972cc2_8',);
                }

                // _assetic_c972cc2_9
                if ('/js/c972cc2_jquery.Jcrop.min_10.js' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'c972cc2',  'pos' => 9,  '_format' => 'js',  '_route' => '_assetic_c972cc2_9',);
                }

                // _assetic_c972cc2_10
                if ('/js/c972cc2_pnotify.custom.min_11.js' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'c972cc2',  'pos' => 10,  '_format' => 'js',  '_route' => '_assetic_c972cc2_10',);
                }

                // _assetic_c972cc2_11
                if ('/js/c972cc2_general_12.js' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'c972cc2',  'pos' => 11,  '_format' => 'js',  '_route' => '_assetic_c972cc2_11',);
                }

                // _assetic_c972cc2_12
                if ('/js/c972cc2_frontend_13.js' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'c972cc2',  'pos' => 12,  '_format' => 'js',  '_route' => '_assetic_c972cc2_12',);
                }

                // _assetic_c972cc2_13
                if ('/js/c972cc2_ed_autocomplete_14.js' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'c972cc2',  'pos' => 13,  '_format' => 'js',  '_route' => '_assetic_c972cc2_13',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/css/4c966cd')) {
            // _assetic_4c966cd
            if ('/css/4c966cd.css' === $pathinfo) {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '4c966cd',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_4c966cd',);
            }

            // _assetic_4c966cd_0
            if ('/css/4c966cd_service_1.css' === $pathinfo) {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '4c966cd',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_4c966cd_0',);
            }

        }

        if (0 === strpos($pathinfo, '/js/compiled_d569369')) {
            // _assetic_d569369
            if ('/js/compiled_d569369.js' === $pathinfo) {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'd569369',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_d569369',);
            }

            // _assetic_d569369_0
            if ('/js/compiled_d569369_parallax.min_1.js' === $pathinfo) {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'd569369',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_d569369_0',);
            }

        }

        if (0 === strpos($pathinfo, '/css')) {
            if (0 === strpos($pathinfo, '/css/5bc8949')) {
                // _assetic_5bc8949
                if ('/css/5bc8949.css' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '5bc8949',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_5bc8949',);
                }

                if (0 === strpos($pathinfo, '/css/5bc8949_')) {
                    // _assetic_5bc8949_0
                    if ('/css/5bc8949_service_1.css' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '5bc8949',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_5bc8949_0',);
                    }

                    // _assetic_5bc8949_1
                    if ('/css/5bc8949_contact_2.css' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '5bc8949',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_5bc8949_1',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/css/c7974be')) {
                // _assetic_c7974be
                if ('/css/c7974be.css' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'c7974be',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_c7974be',);
                }

                if (0 === strpos($pathinfo, '/css/c7974be_')) {
                    if (0 === strpos($pathinfo, '/css/c7974be_style')) {
                        // _assetic_c7974be_0
                        if ('/css/c7974be_style_common_1.css' === $pathinfo) {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => 'c7974be',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_c7974be_0',);
                        }

                        // _assetic_c7974be_1
                        if ('/css/c7974be_style10_2.css' === $pathinfo) {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => 'c7974be',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_c7974be_1',);
                        }

                    }

                    if (0 === strpos($pathinfo, '/css/c7974be_owl.')) {
                        // _assetic_c7974be_2
                        if ('/css/c7974be_owl.carousel.min_3.css' === $pathinfo) {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => 'c7974be',  'pos' => 2,  '_format' => 'css',  '_route' => '_assetic_c7974be_2',);
                        }

                        // _assetic_c7974be_3
                        if ('/css/c7974be_owl.theme.default.min_4.css' === $pathinfo) {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => 'c7974be',  'pos' => 3,  '_format' => 'css',  '_route' => '_assetic_c7974be_3',);
                        }

                    }

                }

            }

        }

        if (0 === strpos($pathinfo, '/js/compiled_5d8e403')) {
            // _assetic_5d8e403
            if ('/js/compiled_5d8e403.js' === $pathinfo) {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '5d8e403',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_5d8e403',);
            }

            if (0 === strpos($pathinfo, '/js/compiled_5d8e403_')) {
                // _assetic_5d8e403_0
                if ('/js/compiled_5d8e403_owl.carousel.min_1.js' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '5d8e403',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_5d8e403_0',);
                }

                // _assetic_5d8e403_1
                if ('/js/compiled_5d8e403_jquery.cbslideheader.min_2.js' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '5d8e403',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_5d8e403_1',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/css')) {
            if (0 === strpos($pathinfo, '/css/896480c')) {
                // _assetic_896480c
                if ('/css/896480c.css' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '896480c',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_896480c',);
                }

                if (0 === strpos($pathinfo, '/css/896480c_s')) {
                    // _assetic_896480c_0
                    if ('/css/896480c_service_1.css' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '896480c',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_896480c_0',);
                    }

                    if (0 === strpos($pathinfo, '/css/896480c_style')) {
                        // _assetic_896480c_1
                        if ('/css/896480c_style_common_2.css' === $pathinfo) {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '896480c',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_896480c_1',);
                        }

                        // _assetic_896480c_2
                        if ('/css/896480c_style10_3.css' === $pathinfo) {
                            return array (  '_controller' => 'assetic.controller:render',  'name' => '896480c',  'pos' => 2,  '_format' => 'css',  '_route' => '_assetic_896480c_2',);
                        }

                    }

                }

            }

            if (0 === strpos($pathinfo, '/css/083a9db')) {
                // _assetic_083a9db
                if ('/css/083a9db.css' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '083a9db',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_083a9db',);
                }

                if (0 === strpos($pathinfo, '/css/083a9db_')) {
                    // _assetic_083a9db_0
                    if ('/css/083a9db_lightbox_1.css' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '083a9db',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_083a9db_0',);
                    }

                    // _assetic_083a9db_1
                    if ('/css/083a9db_right_2.css' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '083a9db',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_083a9db_1',);
                    }

                    // _assetic_083a9db_2
                    if ('/css/083a9db_service_3.css' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '083a9db',  'pos' => 2,  '_format' => 'css',  '_route' => '_assetic_083a9db_2',);
                    }

                }

            }

        }

        if (0 === strpos($pathinfo, '/js/compiled_71f2941')) {
            // _assetic_71f2941
            if ('/js/compiled_71f2941.js' === $pathinfo) {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '71f2941',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_71f2941',);
            }

            // _assetic_71f2941_0
            if ('/js/compiled_71f2941_lightbox_1.js' === $pathinfo) {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '71f2941',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_71f2941_0',);
            }

        }

        if (0 === strpos($pathinfo, '/css/5676372')) {
            // _assetic_5676372
            if ('/css/5676372.css' === $pathinfo) {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 5676372,  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_5676372',);
            }

            if (0 === strpos($pathinfo, '/css/5676372_')) {
                // _assetic_5676372_0
                if ('/css/5676372_bootstrap_1.css' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 5676372,  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_5676372_0',);
                }

                // _assetic_5676372_1
                if ('/css/5676372_fa_2.css' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 5676372,  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_5676372_1',);
                }

                // _assetic_5676372_2
                if ('/css/5676372_header_3.css' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 5676372,  'pos' => 2,  '_format' => 'css',  '_route' => '_assetic_5676372_2',);
                }

                // _assetic_5676372_3
                if ('/css/5676372_main_4.css' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 5676372,  'pos' => 3,  '_format' => 'css',  '_route' => '_assetic_5676372_3',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/js/compiled_')) {
            if (0 === strpos($pathinfo, '/js/compiled_7180c87')) {
                // _assetic_7180c87
                if ('/js/compiled_7180c87.js' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '7180c87',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_7180c87',);
                }

                if (0 === strpos($pathinfo, '/js/compiled_7180c87_')) {
                    // _assetic_7180c87_0
                    if ('/js/compiled_7180c87_jquery-2.1.0.min_1.js' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '7180c87',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_7180c87_0',);
                    }

                    // _assetic_7180c87_1
                    if ('/js/compiled_7180c87_router_2.js' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '7180c87',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_7180c87_1',);
                    }

                    // _assetic_7180c87_2
                    if ('/js/compiled_7180c87_loadingRoute_3.js' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '7180c87',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_7180c87_2',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/js/compiled_3f914b1')) {
                // _assetic_3f914b1
                if ('/js/compiled_3f914b1.js' === $pathinfo) {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '3f914b1',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_3f914b1',);
                }

                if (0 === strpos($pathinfo, '/js/compiled_3f914b1_')) {
                    // _assetic_3f914b1_0
                    if ('/js/compiled_3f914b1_scrollReveal.min_1.js' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '3f914b1',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_3f914b1_0',);
                    }

                    // _assetic_3f914b1_1
                    if ('/js/compiled_3f914b1_bootstrap.min_2.js' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '3f914b1',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_3f914b1_1',);
                    }

                    // _assetic_3f914b1_2
                    if ('/js/compiled_3f914b1_classie_3.js' === $pathinfo) {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '3f914b1',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_3f914b1_2',);
                    }

                }

            }

        }

        // home
        if ('' === rtrim($pathinfo, '/')) {
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                goto not_home;
            } else {
                return $this->redirect($rawPathinfo.'/', 'home');
            }

            return array (  '_controller' => 'Blog\\ArticleBundle\\Controller\\PublicController::indexAction',  '_route' => 'home',);
        }
        not_home:

        // service
        if ('/service' === $pathinfo) {
            return array (  '_controller' => 'Blog\\ArticleBundle\\Controller\\PublicController::serviceAction',  '_route' => 'service',);
        }

        // produit
        if ('/produit' === $pathinfo) {
            return array (  '_controller' => 'Blog\\ArticleBundle\\Controller\\PublicController::produitAction',  '_route' => 'produit',);
        }

        // ref
        if ('/reference' === $pathinfo) {
            return array (  '_controller' => 'Blog\\ArticleBundle\\Controller\\PublicController::refAction',  '_route' => 'ref',);
        }

        // contact
        if ('/contact' === $pathinfo) {
            return array (  '_controller' => 'Blog\\ArticleBundle\\Controller\\PublicController::contactAction',  '_route' => 'contact',);
        }

        // apropos
        if ('/apropos' === $pathinfo) {
            return array (  '_controller' => 'Blog\\ArticleBundle\\Controller\\PublicController::aproposAction',  '_route' => 'apropos',);
        }

        // product
        if (0 === strpos($pathinfo, '/produit') && preg_match('#^/produit/(?P<page>\\d+)/(?P<slug>[^/]++)$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'product')), array (  '_controller' => 'Blog\\ArticleBundle\\Controller\\PublicController::product_articleAction',  'page' => 1,));
        }

        // fos_js_routing_js
        if (0 === strpos($pathinfo, '/js/routing') && preg_match('#^/js/routing(?:\\.(?P<_format>js|json))?$#sD', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_fos_js_routing_js;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_js_routing_js')), array (  '_controller' => 'fos_js_routing.controller:indexAction',  '_format' => 'js',));
        }
        not_fos_js_routing_js:

        if (0 === strpos($pathinfo, '/admin')) {
            // sonata_admin_redirect
            if ('/admin' === rtrim($pathinfo, '/')) {
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                    goto not_sonata_admin_redirect;
                } else {
                    return $this->redirect($rawPathinfo.'/', 'sonata_admin_redirect');
                }

                return array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController::redirectAction',  'route' => 'sonata_admin_dashboard',  'permanent' => 'true',  '_route' => 'sonata_admin_redirect',);
            }
            not_sonata_admin_redirect:

            // sonata_admin_dashboard
            if ('/admin/dashboard' === $pathinfo) {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::dashboardAction',  '_route' => 'sonata_admin_dashboard',);
            }

            if (0 === strpos($pathinfo, '/admin/core')) {
                // sonata_admin_retrieve_form_element
                if ('/admin/core/get-form-field-element' === $pathinfo) {
                    return array (  '_controller' => 'sonata.admin.controller.admin:retrieveFormFieldElementAction',  '_route' => 'sonata_admin_retrieve_form_element',);
                }

                // sonata_admin_append_form_element
                if ('/admin/core/append-form-field-element' === $pathinfo) {
                    return array (  '_controller' => 'sonata.admin.controller.admin:appendFormFieldElementAction',  '_route' => 'sonata_admin_append_form_element',);
                }

                // sonata_admin_short_object_information
                if (0 === strpos($pathinfo, '/admin/core/get-short-object-description') && preg_match('#^/admin/core/get\\-short\\-object\\-description(?:\\.(?P<_format>html|json))?$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_short_object_information')), array (  '_controller' => 'sonata.admin.controller.admin:getShortObjectDescriptionAction',  '_format' => 'html',));
                }

                // sonata_admin_set_object_field_value
                if ('/admin/core/set-object-field-value' === $pathinfo) {
                    return array (  '_controller' => 'sonata.admin.controller.admin:setObjectFieldValueAction',  '_route' => 'sonata_admin_set_object_field_value',);
                }

            }

            // sonata_admin_search
            if ('/admin/search' === $pathinfo) {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::searchAction',  '_route' => 'sonata_admin_search',);
            }

            // sonata_admin_retrieve_autocomplete_items
            if ('/admin/core/get-autocomplete-items' === $pathinfo) {
                return array (  '_controller' => 'sonata.admin.controller.admin:retrieveAutocompleteItemsAction',  '_route' => 'sonata_admin_retrieve_autocomplete_items',);
            }

            if (0 === strpos($pathinfo, '/admin/blog/article')) {
                if (0 === strpos($pathinfo, '/admin/blog/article/slide')) {
                    // admin_blog_article_slide_list
                    if ('/admin/blog/article/slide/list' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.slide',  '_sonata_name' => 'admin_blog_article_slide_list',  '_route' => 'admin_blog_article_slide_list',);
                    }

                    // admin_blog_article_slide_create
                    if ('/admin/blog/article/slide/create' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.slide',  '_sonata_name' => 'admin_blog_article_slide_create',  '_route' => 'admin_blog_article_slide_create',);
                    }

                    // admin_blog_article_slide_batch
                    if ('/admin/blog/article/slide/batch' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.slide',  '_sonata_name' => 'admin_blog_article_slide_batch',  '_route' => 'admin_blog_article_slide_batch',);
                    }

                    // admin_blog_article_slide_edit
                    if (preg_match('#^/admin/blog/article/slide/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_slide_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.slide',  '_sonata_name' => 'admin_blog_article_slide_edit',));
                    }

                    // admin_blog_article_slide_delete
                    if (preg_match('#^/admin/blog/article/slide/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_slide_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.slide',  '_sonata_name' => 'admin_blog_article_slide_delete',));
                    }

                    // admin_blog_article_slide_show
                    if (preg_match('#^/admin/blog/article/slide/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_slide_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.slide',  '_sonata_name' => 'admin_blog_article_slide_show',));
                    }

                    // admin_blog_article_slide_export
                    if ('/admin/blog/article/slide/export' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.slide',  '_sonata_name' => 'admin_blog_article_slide_export',  '_route' => 'admin_blog_article_slide_export',);
                    }

                }

                if (0 === strpos($pathinfo, '/admin/blog/article/category')) {
                    // admin_blog_article_category_list
                    if ('/admin/blog/article/category/list' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.category',  '_sonata_name' => 'admin_blog_article_category_list',  '_route' => 'admin_blog_article_category_list',);
                    }

                    // admin_blog_article_category_create
                    if ('/admin/blog/article/category/create' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.category',  '_sonata_name' => 'admin_blog_article_category_create',  '_route' => 'admin_blog_article_category_create',);
                    }

                    // admin_blog_article_category_batch
                    if ('/admin/blog/article/category/batch' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.category',  '_sonata_name' => 'admin_blog_article_category_batch',  '_route' => 'admin_blog_article_category_batch',);
                    }

                    // admin_blog_article_category_edit
                    if (preg_match('#^/admin/blog/article/category/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_category_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.category',  '_sonata_name' => 'admin_blog_article_category_edit',));
                    }

                    // admin_blog_article_category_delete
                    if (preg_match('#^/admin/blog/article/category/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_category_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.category',  '_sonata_name' => 'admin_blog_article_category_delete',));
                    }

                    // admin_blog_article_category_show
                    if (preg_match('#^/admin/blog/article/category/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_category_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.category',  '_sonata_name' => 'admin_blog_article_category_show',));
                    }

                    // admin_blog_article_category_export
                    if ('/admin/blog/article/category/export' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.category',  '_sonata_name' => 'admin_blog_article_category_export',  '_route' => 'admin_blog_article_category_export',);
                    }

                }

                if (0 === strpos($pathinfo, '/admin/blog/article/produit')) {
                    // admin_blog_article_produit_list
                    if ('/admin/blog/article/produit/list' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.produit',  '_sonata_name' => 'admin_blog_article_produit_list',  '_route' => 'admin_blog_article_produit_list',);
                    }

                    // admin_blog_article_produit_create
                    if ('/admin/blog/article/produit/create' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.produit',  '_sonata_name' => 'admin_blog_article_produit_create',  '_route' => 'admin_blog_article_produit_create',);
                    }

                    // admin_blog_article_produit_batch
                    if ('/admin/blog/article/produit/batch' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.produit',  '_sonata_name' => 'admin_blog_article_produit_batch',  '_route' => 'admin_blog_article_produit_batch',);
                    }

                    // admin_blog_article_produit_edit
                    if (preg_match('#^/admin/blog/article/produit/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_produit_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.produit',  '_sonata_name' => 'admin_blog_article_produit_edit',));
                    }

                    // admin_blog_article_produit_delete
                    if (preg_match('#^/admin/blog/article/produit/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_produit_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.produit',  '_sonata_name' => 'admin_blog_article_produit_delete',));
                    }

                    // admin_blog_article_produit_show
                    if (preg_match('#^/admin/blog/article/produit/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_produit_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.produit',  '_sonata_name' => 'admin_blog_article_produit_show',));
                    }

                    // admin_blog_article_produit_export
                    if ('/admin/blog/article/produit/export' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.produit',  '_sonata_name' => 'admin_blog_article_produit_export',  '_route' => 'admin_blog_article_produit_export',);
                    }

                }

                if (0 === strpos($pathinfo, '/admin/blog/article/requete')) {
                    // admin_blog_article_requete_list
                    if ('/admin/blog/article/requete/list' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.message',  '_sonata_name' => 'admin_blog_article_requete_list',  '_route' => 'admin_blog_article_requete_list',);
                    }

                    // admin_blog_article_requete_create
                    if ('/admin/blog/article/requete/create' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.message',  '_sonata_name' => 'admin_blog_article_requete_create',  '_route' => 'admin_blog_article_requete_create',);
                    }

                    // admin_blog_article_requete_batch
                    if ('/admin/blog/article/requete/batch' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.message',  '_sonata_name' => 'admin_blog_article_requete_batch',  '_route' => 'admin_blog_article_requete_batch',);
                    }

                    // admin_blog_article_requete_edit
                    if (preg_match('#^/admin/blog/article/requete/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_requete_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.message',  '_sonata_name' => 'admin_blog_article_requete_edit',));
                    }

                    // admin_blog_article_requete_delete
                    if (preg_match('#^/admin/blog/article/requete/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_requete_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.message',  '_sonata_name' => 'admin_blog_article_requete_delete',));
                    }

                    // admin_blog_article_requete_show
                    if (preg_match('#^/admin/blog/article/requete/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_blog_article_requete_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.message',  '_sonata_name' => 'admin_blog_article_requete_show',));
                    }

                    // admin_blog_article_requete_export
                    if ('/admin/blog/article/requete/export' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.message',  '_sonata_name' => 'admin_blog_article_requete_export',  '_route' => 'admin_blog_article_requete_export',);
                    }

                }

            }

            if (0 === strpos($pathinfo, '/admin/sonata/media')) {
                if (0 === strpos($pathinfo, '/admin/sonata/media/media')) {
                    // admin_sonata_media_media_list
                    if ('/admin/sonata/media/media/list' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::listAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_list',  '_route' => 'admin_sonata_media_media_list',);
                    }

                    // admin_sonata_media_media_create
                    if ('/admin/sonata/media/media/create' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::createAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_create',  '_route' => 'admin_sonata_media_media_create',);
                    }

                    // admin_sonata_media_media_batch
                    if ('/admin/sonata/media/media/batch' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::batchAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_batch',  '_route' => 'admin_sonata_media_media_batch',);
                    }

                    // admin_sonata_media_media_edit
                    if (preg_match('#^/admin/sonata/media/media/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_media_edit')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::editAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_edit',));
                    }

                    // admin_sonata_media_media_delete
                    if (preg_match('#^/admin/sonata/media/media/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_media_delete')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::deleteAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_delete',));
                    }

                    // admin_sonata_media_media_show
                    if (preg_match('#^/admin/sonata/media/media/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_media_show')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::showAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_show',));
                    }

                    // admin_sonata_media_media_export
                    if ('/admin/sonata/media/media/export' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaAdminController::exportAction',  '_sonata_admin' => 'sonata.media.admin.media',  '_sonata_name' => 'admin_sonata_media_media_export',  '_route' => 'admin_sonata_media_media_export',);
                    }

                }

                if (0 === strpos($pathinfo, '/admin/sonata/media/gallery')) {
                    // admin_sonata_media_gallery_list
                    if ('/admin/sonata/media/gallery/list' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::listAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_list',  '_route' => 'admin_sonata_media_gallery_list',);
                    }

                    // admin_sonata_media_gallery_create
                    if ('/admin/sonata/media/gallery/create' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::createAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_create',  '_route' => 'admin_sonata_media_gallery_create',);
                    }

                    // admin_sonata_media_gallery_batch
                    if ('/admin/sonata/media/gallery/batch' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::batchAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_batch',  '_route' => 'admin_sonata_media_gallery_batch',);
                    }

                    // admin_sonata_media_gallery_edit
                    if (preg_match('#^/admin/sonata/media/gallery/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_gallery_edit')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::editAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_edit',));
                    }

                    // admin_sonata_media_gallery_delete
                    if (preg_match('#^/admin/sonata/media/gallery/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_gallery_delete')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::deleteAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_delete',));
                    }

                    // admin_sonata_media_gallery_show
                    if (preg_match('#^/admin/sonata/media/gallery/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_gallery_show')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::showAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_show',));
                    }

                    // admin_sonata_media_gallery_export
                    if ('/admin/sonata/media/gallery/export' === $pathinfo) {
                        return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryAdminController::exportAction',  '_sonata_admin' => 'sonata.media.admin.gallery',  '_sonata_name' => 'admin_sonata_media_gallery_export',  '_route' => 'admin_sonata_media_gallery_export',);
                    }

                    if (0 === strpos($pathinfo, '/admin/sonata/media/galleryhasmedia')) {
                        // admin_sonata_media_galleryhasmedia_list
                        if ('/admin/sonata/media/galleryhasmedia/list' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_list',  '_route' => 'admin_sonata_media_galleryhasmedia_list',);
                        }

                        // admin_sonata_media_galleryhasmedia_create
                        if ('/admin/sonata/media/galleryhasmedia/create' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_create',  '_route' => 'admin_sonata_media_galleryhasmedia_create',);
                        }

                        // admin_sonata_media_galleryhasmedia_batch
                        if ('/admin/sonata/media/galleryhasmedia/batch' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_batch',  '_route' => 'admin_sonata_media_galleryhasmedia_batch',);
                        }

                        // admin_sonata_media_galleryhasmedia_edit
                        if (preg_match('#^/admin/sonata/media/galleryhasmedia/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_galleryhasmedia_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_edit',));
                        }

                        // admin_sonata_media_galleryhasmedia_delete
                        if (preg_match('#^/admin/sonata/media/galleryhasmedia/(?P<id>[^/]++)/delete$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_galleryhasmedia_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_delete',));
                        }

                        // admin_sonata_media_galleryhasmedia_show
                        if (preg_match('#^/admin/sonata/media/galleryhasmedia/(?P<id>[^/]++)/show$#sD', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_sonata_media_galleryhasmedia_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_show',));
                        }

                        // admin_sonata_media_galleryhasmedia_export
                        if ('/admin/sonata/media/galleryhasmedia/export' === $pathinfo) {
                            return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.media.admin.gallery_has_media',  '_sonata_name' => 'admin_sonata_media_galleryhasmedia_export',  '_route' => 'admin_sonata_media_galleryhasmedia_export',);
                        }

                    }

                }

            }

        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ('/login' === $pathinfo) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }
                not_fos_user_security_login:

                // fos_user_security_check
                if ('/login_check' === $pathinfo) {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ('/logout' === $pathinfo) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_security_logout;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }
            not_fos_user_security_logout:

        }

        if (0 === strpos($pathinfo, '/media')) {
            if (0 === strpos($pathinfo, '/media/gallery')) {
                // sonata_media_gallery_index
                if ('/media/gallery' === rtrim($pathinfo, '/')) {
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                        goto not_sonata_media_gallery_index;
                    } else {
                        return $this->redirect($rawPathinfo.'/', 'sonata_media_gallery_index');
                    }

                    return array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryController::indexAction',  '_route' => 'sonata_media_gallery_index',);
                }
                not_sonata_media_gallery_index:

                // sonata_media_gallery_view
                if (0 === strpos($pathinfo, '/media/gallery/view') && preg_match('#^/media/gallery/view/(?P<id>.*)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_media_gallery_view')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\GalleryController::viewAction',));
                }

            }

            // sonata_media_view
            if (0 === strpos($pathinfo, '/media/view') && preg_match('#^/media/view/(?P<id>[^/]++)(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_media_view')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaController::viewAction',  'format' => 'reference',));
            }

            // sonata_media_download
            if (0 === strpos($pathinfo, '/media/download') && preg_match('#^/media/download/(?P<id>.*)(?:/(?P<format>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_media_download')), array (  '_controller' => 'Sonata\\MediaBundle\\Controller\\MediaController::downloadAction',  'format' => 'reference',));
            }

        }

        // ed_blog_admin_feed
        if (0 === strpos($pathinfo, '/feed') && preg_match('#^/feed/(?P<type>[^/]++)$#sD', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_admin_feed')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\FeedController::feedAction',));
        }

        if (0 === strpos($pathinfo, '/all-news')) {
            // ed_blog_homepage
            if ('/all-news' === rtrim($pathinfo, '/')) {
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                    goto not_ed_blog_homepage;
                } else {
                    return $this->redirect($rawPathinfo.'/', 'ed_blog_homepage');
                }

                return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\BlogController::indexAction',  '_route' => 'ed_blog_homepage',);
            }
            not_ed_blog_homepage:

            if (0 === strpos($pathinfo, '/all-news/blog')) {
                // ed_blog_frontend_index
                if ('/all-news/blog' === $pathinfo) {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\BlogController::indexAction',  '_route' => 'ed_blog_frontend_index',);
                }

                // ed_blog_frontend_blog_index
                if ('/all-news/blog' === rtrim($pathinfo, '/')) {
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                        goto not_ed_blog_frontend_blog_index;
                    } else {
                        return $this->redirect($rawPathinfo.'/', 'ed_blog_frontend_blog_index');
                    }

                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\BlogController::indexAction',  '_route' => 'ed_blog_frontend_blog_index',);
                }
                not_ed_blog_frontend_blog_index:

                // ed_frontend_blog_single_article
                if (preg_match('#^/all\\-news/blog/(?P<slug>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_frontend_blog_single_article')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\BlogController::singleArticleAction',));
                }

                // ed_frontend_blog_by_category
                if (0 === strpos($pathinfo, '/all-news/blog/category') && preg_match('#^/all\\-news/blog/category/(?P<categorySlug>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_frontend_blog_by_category')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\BlogController::byCategoryAction',));
                }

                // ed_frontend_blog_by_tag
                if (0 === strpos($pathinfo, '/all-news/blog/tag') && preg_match('#^/all\\-news/blog/tag/(?P<tagSlug>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_frontend_blog_by_tag')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\BlogController::byTagAction',));
                }

                if (0 === strpos($pathinfo, '/all-news/blog/a')) {
                    // ed_frontend_blog_by_author
                    if (0 === strpos($pathinfo, '/all-news/blog/author') && preg_match('#^/all\\-news/blog/author/(?P<username>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_frontend_blog_by_author')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\BlogController::byAuthorAction',));
                    }

                    // ed_frontend_blog_archive
                    if (0 === strpos($pathinfo, '/all-news/blog/archive') && preg_match('#^/all\\-news/blog/archive/(?P<yearMonth>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_frontend_blog_archive')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\BlogController::archiveAction',));
                    }

                }

            }

            // ed_blog_redirect_authenticate
            if ('/all-news/authenticate' === $pathinfo) {
                return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\RedirectController::authenticateAction',  '_route' => 'ed_blog_redirect_authenticate',);
            }

            // ed_blog_redirect_login
            if ('/all-news/login' === $pathinfo && $request->isXmlHttpRequest()) {
                return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Frontend\\RedirectController::ajaxLoginAction',  '_route' => 'ed_blog_redirect_login',);
            }

        }

        if (0 === strpos($pathinfo, '/news/admin')) {
            if (0 === strpos($pathinfo, '/news/admin/article')) {
                // ed_blog_admin_article_create
                if ('/news/admin/article/create' === $pathinfo) {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::createAction',  '_route' => 'ed_blog_admin_article_create',);
                }

                // ed_blog_admin_article_edit
                if (0 === strpos($pathinfo, '/news/admin/article/edit') && preg_match('#^/news/admin/article/edit/(?P<slug>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_admin_article_edit')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::editAction',));
                }

                // ed_blog_article_autosave
                if (0 === strpos($pathinfo, '/news/admin/article/autosave') && preg_match('#^/news/admin/article/autosave/(?P<slug>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_article_autosave')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::autosaveAction',));
                }

                // ed_blog_admin_article_media_list
                if ('/news/admin/article/media/list' === $pathinfo) {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::mediaListAction',  '_route' => 'ed_blog_admin_article_media_list',);
                }

            }

            // ed_blog_admin_article_upload
            if ('/news/admin/upload' === $pathinfo) {
                return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::uploadAction',  '_route' => 'ed_blog_admin_article_upload',);
            }

            if (0 === strpos($pathinfo, '/news/admin/article')) {
                // ed_blog_admin_article_list
                if ('/news/admin/article/list' === $pathinfo) {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::listAction',  '_route' => 'ed_blog_admin_article_list',);
                }

                // ed_blog_admin_article_show
                if (0 === strpos($pathinfo, '/news/admin/article/show') && preg_match('#^/news/admin/article/show/(?P<slug>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_admin_article_show')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::showAction',));
                }

                // ed_blog_admin_article_delete
                if (preg_match('#^/news/admin/article/(?P<slug>[^/]++)/remove$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_admin_article_delete')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::deleteAction',));
                }

                // ed_blog_article_check_writing_lock
                if (preg_match('#^/news/admin/article/(?P<id>[^/]++)/check$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_article_check_writing_lock')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::checkWritingLockAction',));
                }

                // ed_blog_article_takeover
                if (preg_match('#^/news/admin/article/(?P<id>[^/]++)/takeover$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_article_takeover')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\ArticleController::takeoverAction',));
                }

            }

            if (0 === strpos($pathinfo, '/news/admin/c')) {
                if (0 === strpos($pathinfo, '/news/admin/category')) {
                    // ed_blog_category_list
                    if ('/news/admin/category/list' === $pathinfo) {
                        return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CategoryController::listAction',  '_route' => 'ed_blog_category_list',);
                    }

                    // ed_blog_category_create
                    if ('/news/admin/category/create' === $pathinfo) {
                        return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CategoryController::createAction',  '_route' => 'ed_blog_category_create',);
                    }

                    // ed_blog_category_edit
                    if (preg_match('#^/news/admin/category/(?P<slug>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_category_edit')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CategoryController::editAction',));
                    }

                    // ed_blog_category_delete
                    if (preg_match('#^/news/admin/category/(?P<slug>[^/]++)/remove$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_category_delete')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CategoryController::deleteAction',));
                    }

                    // ed_blog_category_all_pretty
                    if (preg_match('#^/news/admin/category/(?P<template>[^/]++)/pretty$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_category_all_pretty')), array (  'template' => 'all',  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CategoryController::allPrettyAction',));
                    }

                }

                if (0 === strpos($pathinfo, '/news/admin/comment')) {
                    // ed_blog_admin_comment_create
                    if (preg_match('#^/news/admin/comment/(?P<slug>[^/]++)/create$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_admin_comment_create')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CommentController::createAction',));
                    }

                    // ed_blog_admin_comment_list
                    if ('/news/admin/comment/list' === $pathinfo) {
                        return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CommentController::listAction',  '_route' => 'ed_blog_admin_comment_list',);
                    }

                    // ed_blog_comment_delete
                    if (preg_match('#^/news/admin/comment/(?P<id>[^/]++)/remove$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_comment_delete')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CommentController::deleteAction',));
                    }

                    // ed_blog_comment_edit_status
                    if (preg_match('#^/news/admin/comment/(?P<id>[^/]++)/status/(?P<status>0|1)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_comment_edit_status')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CommentController::editStatusAction',));
                    }

                    // ed_blog_comment_edit
                    if (preg_match('#^/news/admin/comment/(?P<id>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_comment_edit')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\CommentController::editAction',));
                    }

                }

            }

            // ed_blog_homepage_index
            if ('/news/admin' === rtrim($pathinfo, '/')) {
                if ('/' === substr($pathinfo, -1)) {
                    // no-op
                } elseif (!in_array($this->context->getMethod(), array('HEAD', 'GET'))) {
                    goto not_ed_blog_homepage_index;
                } else {
                    return $this->redirect($rawPathinfo.'/', 'ed_blog_homepage_index');
                }

                return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\HomepageController::indexAction',  '_route' => 'ed_blog_homepage_index',);
            }
            not_ed_blog_homepage_index:

            if (0 === strpos($pathinfo, '/news/admin/media')) {
                // ed_blog_admin_media_list
                if ('/news/admin/media/list' === $pathinfo) {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\MediaController::mediaListAction',  '_route' => 'ed_blog_admin_media_list',);
                }

                if (0 === strpos($pathinfo, '/news/admin/media/upload')) {
                    // ed_blog_admin_media_upload
                    if ('/news/admin/media/upload' === $pathinfo) {
                        return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\MediaController::uploadAction',  '_route' => 'ed_blog_admin_media_upload',);
                    }

                    // ed_blog_admin_media_upload_excerpt
                    if ('/news/admin/media/upload/excerpt' === $pathinfo) {
                        return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\MediaController::uploadExcerptAction',  '_route' => 'ed_blog_admin_media_upload_excerpt',);
                    }

                }

                // ed_blog_admin_media_delete
                if (0 === strpos($pathinfo, '/news/admin/media/delete') && preg_match('#^/news/admin/media/delete/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_admin_media_delete')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\MediaController::deleteAction',));
                }

                if (0 === strpos($pathinfo, '/news/admin/media/edit')) {
                    // ed_blog_admin_media_edit_info
                    if (0 === strpos($pathinfo, '/news/admin/media/edit-info') && preg_match('#^/news/admin/media/edit\\-info/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_admin_media_edit_info')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\MediaController::editInfoAction',));
                    }

                    // ed_blog_admin_media_edit
                    if (preg_match('#^/news/admin/media/edit/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_admin_media_edit')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\MediaController::editAction',));
                    }

                }

                // ed_blog_admin_media_crop
                if (0 === strpos($pathinfo, '/news/admin/media/crop') && preg_match('#^/news/admin/media/crop/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_admin_media_crop')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\MediaController::cropAction',));
                }

            }

            if (0 === strpos($pathinfo, '/news/admin/settings')) {
                // ed_blog_admin_settings_edit
                if ('/news/admin/settings/edit' === $pathinfo) {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\SettingsController::editAction',  '_route' => 'ed_blog_admin_settings_edit',);
                }

                // ed_blog_admin_settings_show_format
                if ('/news/admin/settings/showDateTime' === $pathinfo) {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\SettingsController::showFormat',  '_route' => 'ed_blog_admin_settings_show_format',);
                }

            }

            if (0 === strpos($pathinfo, '/news/admin/tag')) {
                // ed_blog_tag_express_create
                if ('/news/admin/tag/express-create' === $pathinfo) {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\TagController::createExpressAction',  '_route' => 'ed_blog_tag_express_create',);
                }

                // ed_blog_tag_list
                if ('/news/admin/tag/list' === $pathinfo) {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\TagController::listAction',  '_route' => 'ed_blog_tag_list',);
                }

                // ed_blog_tag_create
                if ('/news/admin/tag/create' === $pathinfo) {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\TagController::createAction',  '_route' => 'ed_blog_tag_create',);
                }

                // ed_blog_tag_edit
                if (preg_match('#^/news/admin/tag/(?P<slug>[^/]++)/edit$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_tag_edit')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\TagController::editAction',));
                }

                // ed_blog_tag_delete
                if (preg_match('#^/news/admin/tag/(?P<slug>[^/]++)/remove$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_tag_delete')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\TagController::deleteAction',));
                }

                // ed_blog_tags_top_tags
                if (0 === strpos($pathinfo, '/news/admin/tag/toptags') && preg_match('#^/news/admin/tag/toptags/(?P<number>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_tags_top_tags')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\TagController::topTagsAction',));
                }

            }

            if (0 === strpos($pathinfo, '/news/admin/user')) {
                // ed_blog_user_list
                if ('/news/admin/user/list' === $pathinfo) {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\UserController::listAction',  '_route' => 'ed_blog_user_list',);
                }

                // ed_blog_user_add
                if ('/news/admin/user/add' === $pathinfo) {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\UserController::addAction',  '_route' => 'ed_blog_user_add',);
                }

                // ed_blog_user_search
                if ('/news/admin/user/search' === $pathinfo) {
                    return array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\UserController::searchAction',  '_route' => 'ed_blog_user_search',);
                }

                // ed_blog_user_edit
                if (0 === strpos($pathinfo, '/news/admin/user/edit') && preg_match('#^/news/admin/user/edit/(?P<username>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_user_edit')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\UserController::editAction',));
                }

                // ed_blog_user_revoke
                if (preg_match('#^/news/admin/user/(?P<username>[^/]++)/revoke$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ed_blog_user_revoke')), array (  '_controller' => 'ED\\BlogBundle\\Controller\\Backend\\UserController::revokeAction',));
                }

            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
