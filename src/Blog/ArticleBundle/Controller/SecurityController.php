<?php

namespace Blog\ArticleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Blog\ArticleBundle\Entity\inscr;
use Blog\ArticleBundle\Entity\user;
use Symfony\component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContext;

class SecurityController extends Controller {

    public function loginAction() {
        if ($this->get('security.context')->isGranted('ROLE_USER')) {

            return $this->redirect($this->generateUrl('bloomUser_homepage'));
        }
        $request = $this->getRequest();
        $session = $request->getSession();

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }


        return $this->render('bloomUserBundle:Public:login.html.twig', array(
                    'error' => $error,
        ));
    }

    public function loginBlogAction() {
        if ($this->get('security.context')->isGranted('ROLE_USER')) {

            return $this->redirect($this->generateUrl('bloomUser_homepage'));
        }
        $request = $this->getRequest();
        $session = $request->getSession();


        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render('bloomUserBundle:Public:loginBlog.html.twig', array(
                    'error' => $error,
        ));
    }

}
