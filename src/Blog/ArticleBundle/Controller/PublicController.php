<?php

namespace Blog\ArticleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ED\BlogBundle\Model\Entity\Taxonomy;

class PublicController extends Controller {

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        //recuperation des derniers articles
        $articles = $em->getRepository('BlogArticleBundle:News')->getActiveArticles(3);
        $categories = $em->getRepository('BlogArticleBundle:category')->getArticle(0,6);
        //slide
        $slides = $em->getRepository('BlogArticleBundle:slide')->findAll();
        // $admin_pool = $this->get('sonata.admin.pool');
        return $this->render('BlogArticleBundle:Public:index.html.twig', array(
                    'slides' => $slides,
                    'categories' => $categories,
                    'articles' => $articles,
        ));
    }

    public function serviceAction() {

        return $this->render('BlogArticleBundle:Public:service.html.twig', array(
        ));
    }
    public function aproposAction() {

        return $this->render('BlogArticleBundle:Public:apropos.html.twig', array(
        ));
    }

    public function contactAction( \Symfony\Component\HttpFoundation\Request $request) {

        $message = new \Blog\ArticleBundle\Entity\requete();
        $form = $this->createForm(new \Blog\ArticleBundle\Form\requeteType($message));

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $message = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($message);
            $em->flush();
            $this->addFlash(
                            'message', 'Votre message a été envoyé. Merci de nous faire confiance.'
                    );
            return $this->redirect($this->generateUrl("contact"));
        }
        return $this->render('BlogArticleBundle:Public:contact.html.twig', array(
        "form" => $form->createView()
        ));
    }

    public function produitAction() {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('BlogArticleBundle:category')->findAll();
        return $this->render('BlogArticleBundle:Public:produit.html.twig', array(
                    'categories' => $categories,
        ));
    }

    public function product_articleAction(\Blog\ArticleBundle\Entity\category $category, $page) {
        $em = $this->getDoctrine()->getManager();
        $limit = 10;
        $produits = $em->getRepository("BlogArticleBundle:produit")->getArticle($category, $page - 1, $limit);
        return $this->render('BlogArticleBundle:Public:produit_article.html.twig', array(
                    'category' => $category,
                    'produits' => $produits,
                    "lastPage" => round($category->getProduit()->count() / $limit, 0, PHP_ROUND_HALF_DOWN),
                    "currentPage" => $page,
                    "showAlwaysFirstAndLast" => false,
                    "paginationPath" => "product",
                    "currentFilters" => array("slug" => $category->getSlug())
        ));
    }

}
