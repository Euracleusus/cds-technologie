<?php

namespace Blog\ArticleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ED\BlogBundle\Model\Entity\Taxonomy;
class ToolController extends Controller {


    public function headerAction($active=null) {
        return $this->render('BlogArticleBundle:Tool:header.html.twig',array(
            "active"=>$active
        ));
    }
    public function footerAction() {
        return $this->render('BlogArticleBundle:Tool:footer.html.twig',array());
    }
    
}
