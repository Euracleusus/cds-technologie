<?php

namespace Blog\ArticleBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
/**
 * Description of articleBlog
 *
 * @author Julien
 */
class RequeteAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper->add('nom', "text", array("max_length" => 53))
                ->add('email', "text", array("max_length" => 53))
                ->add('messages', 'textarea', array("max_length" => 436))
               
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
               ->add('nom', null, array("max_length" => 53))
                ->add('email', null, array("max_length" => 53))
                ->add('dCrea', null, array("label" =>"Date du messages"))
                ->add('updatedAt', null, array("label" =>"Date de Mise a jour"))
                ->add('messages', null, array("max_length" => 436));
    }
   public function configureShowFields(ShowMapper $showMapper) {
        $showMapper
               ->add('nom', null, array("max_length" => 53))
                ->add('email', null, array("max_length" => 53))
                ->add('dCrea', null, array("label" =>"Date du messages"))
                ->add('updatedAt', null, array("label" =>"Date de Mise a jour"))
                ->add('messages', null, array("max_length" => 436))
                ->add('repondu', null, array("editable" => true,"label" => "repondu ?"));
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'delete' => array(),
            )))
                ->addIdentifier('nom', null, array("max_length" => 53))
                  ->add('email', null, array("max_length" => 53))
                ->add('dCrea', null, array("label" =>"Date du messages"))
               ->add('repondu', null, array("editable" => true,"label" => "repondu ?"))

        ;
    }

}
