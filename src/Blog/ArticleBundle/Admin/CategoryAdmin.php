<?php

namespace Blog\ArticleBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Description of articleBlog
 *
 * @author Julien
 */
class CategoryAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper->add('name', "text", array("max_length" => 46,"attr"=>array("placeholder"=>"Entrez le nom de la cateries")))
                ->add('descr', 'textarea', array("max_length" => 162,"attr"=>array("placeholder"=>"Entrez la description de la categories")))
                ->add('image', 'sonata_type_model_list', array('by_reference' => false), array("link_parameters" => array("context" => "product")))

        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('name')
                ->add('descr');
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
            )))
                ->addIdentifier('name')
                ->add('image', null, array('template' => 'BlogArticleBundle:Article:list_image_2.html.twig'))

        ;
    }

}
