<?php

namespace Blog\ArticleBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Description of articleBlog
 *
 * @author Julien
 */
class SlideAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper->add('name', "text", array("max_length" => 53))
                ->add('descr', 'textarea', array("max_length" => 436))
                ->add('lien', 'text')
                ->add('image', 'sonata_type_model_list', array('by_reference' => false), array("link_parameters" => array("context" => "default")))

        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('name',null,array("label"=>"Titre"))
                ->add('descr',null,array("label"=>"Description"));
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
            )))
                ->addIdentifier('name')
                ->add('lien')
                ->add('image', null, array('template' => 'BlogArticleBundle:Article:list_image_2.html.twig'))

        ;
    }

}
