<?php

namespace Blog\ArticleBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * Description of PaysBlog
 *
 * @author Julien
 */
class UserAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('username', "email",array("label"=>"Email"))
                ->add('plainPassword', "password",array("label"=>"Mot de passe"))
                ->add('nom', "text",array("label"=>"Nom"))
                ->add('prenom', "text",array("label"=>"Prenom"))
                
                ->add('Enabled', "checkbox",array("label"=>"Actif"))
                ->add('Roles', 'choice', array('multiple' => true, 'choices' => array('ROLE_USER' => 'Utilisateur Simple', 'ROLE_SUPER_Blog' => 'Blogistrateur', 'ROLE_BLOG_USER' => 'Bloggeur', 'ROLE_BLOG_Blog' => 'Blogistrateur du blog', 'ROLE_BLOG_CONTRIBUTOR' => 'Moderateur du Blog', 'ROLE_BLOG_CONTRIBUTOR' => 'Moderateur du Blog', 'ROLE_BLOG_AUTHOR' => 'Auteur Blog', 'ROLE_BLOG_EDITOR' => 'Editeur du blog'), 'label' => 'Rôles utilisateurs', 'max_length' => '14', 'required' => false,  'label_attr' => array('class' => 'col-md-12 col-xs-12')))
               
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                 ->add('username')
                ->add('nom')
                ->add('prenom')
                ->add('roles')

        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper->addIdentifier('id')
                ->add('username')
                ->add('nom')
                ->add('prenom')
                ->add('dossier')
                ->add('roles', 'choice', array('multiple' => true, 'choices' => array('ROLE_USER' => 'Utilisateur Simple', 'ROLE_SUPER_Blog' => 'Blogistrateur', 'ROLE_BLOG_USER' => 'Bloggeur', 'ROLE_BLOG_CONTRIBUTOR' => 'Moderateur du Blog', 'ROLE_BLOG_CONTRIBUTOR' => 'Moderateur du Blog', 'ROLE_BLOG_AUTHOR' => 'Auteur Blog', 'ROLE_BLOG_EDITOR' => 'Editeur du blog', 'ROLE_BLOG_Blog' => 'Blogistrateur du blog'), 'label' => 'Rôles utilisateurs', 'max_length' => '14', 'required' => false,  'label_attr' => array('class' => 'col-md-12 col-xs-12')))

        ;
    }

}
