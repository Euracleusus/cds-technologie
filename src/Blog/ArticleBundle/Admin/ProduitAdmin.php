<?php

namespace Blog\ArticleBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * Description of articleAdmin
 *
 * @author Julien
 */
class ProduitAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('category', 'sonata_type_model', array('by_reference' => false, "empty_value" => " - Choississez une categorie - ", "multiple" => false, 'class' => 'BlogArticleBundle:category', 'label' => 'Categories', 'attr' => array('placeholder' => 'Choisissez une categories', 'class' => 'category'), 'property' => 'name'))
                ->add('name', 'text', array('max_length' => '60', 'label' => 'Nom de l\'article', 'attr' => array('class' => 'form-control', 'placeholder' => 'Entrez le titre')))
                ->add('descr', 'textarea', array("max_length" => 300, "attr" => array("placeholder" => "Entrez la description du produit")))
                ->add('ref', 'text', array('max_length' => '60', 'label' => 'Reference de l\'article', 'attr' => array('class' => 'form-control', 'placeholder' => 'Entrez la réference de l\'article ')))
                ->add('stock', 'number', array('label' => 'Stock', 'attr' => array('class' => 'form-control', 'placeholder' => 'Entrez le stock ')))
                ->add('prix', 'number', array('label' => 'Prix', "required" => false, 'attr' => array('class' => 'form-control', 'placeholder' => 'Entrez le prix')))
                ->add('imageD', 'sonata_type_model_list', array('by_reference' => false, 'label' => 'Image 1'), array("link_parameters" => array("context" => "product")))
                ->add('image1', 'sonata_type_model_list', array('by_reference' => false, "required" => false, 'label' => 'Image 2'), array("link_parameters" => array("context" => "product")))
                ->add('image2', 'sonata_type_model_list', array('by_reference' => false, "required" => false, 'label' => 'Image 3'), array("link_parameters" => array("context" => "product")))


        ;
    }

    public function configureShowFields(ShowMapper $showMapper) {
        $showMapper
                ->add('image', null, array('template' => 'BlogArticleBundle:article:list_product.html.twig'))
                ->add('category', null, array("label" => "Categorie du produit"))
                ->add('name', null, array("label" => "Nom du produit"))
                ->add('ref', null, array("editable" => true, "label" => "Reference"))
                ->add('descr', null, array("editable" => true, "label" => "Description"))
                ->add('dCrea', null, array("label" => "Date de creation"))
                ->add('dCrea', null, array("label" => "Date de derniere maj"))
                ->add('prix')
                ->add('stock', null, array("editable" => true, "label" => "Quantite en stock"))



        ;
    }

    public function getObjectMetadata($object) {


        return new \Sonata\CoreBundle\Model\Metadata($object->getName(), $object->getFiche(), "/pc/web/" . $object->getImageD()->getLMini());
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('name')
                ->add('category')
                ->add('dCrea')
                ->add('prix')
                ->add('popu')
                ->add('stock')
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper->add('_action', 'actions', array(
                    'actions' => array(
                        'show' => array(),
                        'delete' => array(),
            )))
                ->addIdentifier('name', null, array("editable" => true, "label" => "Designation"))
                ->add('category', null, array("editable" => true))
                ->add('updateAt', "datetime", array("label" => "MAJ"))
                ->add('prix', null, array("editable" => true, "label" => "Prix"))
                ->add('stock')
                ->add('image', null, array('template' => 'BlogArticleBundle:article:list_product.html.twig'))


        ;
    }

}
