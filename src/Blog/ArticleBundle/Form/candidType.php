<?php

namespace Blog\ArticleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class candidType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('annee', 'entity', array("label" => "Niveau*", "attr" => array("class" => "form-control"), 'class' => 'BlogArticleBundle:annee', 'query_builder' => function(\Blog\ArticleBundle\Entity\anneeRepository $er) {
                return $er->createQueryBuilder('u')->orderBy('u.name', 'ASC');
            }, 'label' => 'Annee choisie*', 'property' => 'name'))
                ->add('session', "date", array("label" => "Session*", 'years' => range(date('Y'), date('Y') + 5)))
                ->add('paysChoisie', 'entity', array("label" => "Pays choisie*", "attr" => array("class" => "form-control"), 'class' => 'BlogArticleBundle:pays', 'query_builder' => function(\Blog\ArticleBundle\Entity\paysRepository $er) {
                return $er->createQueryBuilder('u')->orderBy('u.name', 'ASC');
            }, 'label' => 'Pays choisie*', 'property' => 'name'))
                ->add('villeChoisie', "text", array("label" => "Ville choisie", "attr" => array("class" => "form-control", "placeholder" => "Entrez la ville choisie")))
                ->add('ecolChois', "text", array("label" => "Ecole choisie*", "attr" => array("class" => "form-control", "placeholder" => "Entrez le nom de l'ecole choisie")))
                ->add('specia', "text", array("label" => "Specialité*", "attr" => array("class" => "form-control", "placeholder" => "Entrez la specialite choisie")))
                ->add('naiss', "date", array("label" => "Date de naissance", 'years' => range(date('Y') - 30, date('Y') - 16)))
                ->add('lieuNaiss', "text", array("label" => "Lieu de naissance", "attr" => array("class" => "form-control", "placeholder" => "Entrez votre lieu de naissance")))
                ->add('gender', 'choice', array(
                    'choices' => array("label" => "Sexe*","attr" => array("class" => "form-control"),
                        true => 'Homme',
                        false => 'Femme',
                    )
                ))
                ->add('villeAct', "text", array("label" => "Votre ville actuel*", "attr" => array("class" => "form-control", "placeholder" => "Entrez votre ville actuelle")))
                ->add('address', "text", array("label" => "Addresse(Ville-Quartier)*", "attr" => array("class" => "form-control", "placeholder" => "Entrez votre addresse ou votre quartier")))
                ->add('email', "email", array("label" => "Email*", "attr" => array("class" => "form-control", "placeholder" => "Entrez votre addresse electronique")))
                ->add('phone', "text", array("label" => "Numero de telephone*", "attr" => array("class" => "form-control", "placeholder" => "Entrez votre numero de telephone")))
                ->add('cni', "text", array("label" => "Numéro CNI/Passeport*", "attr" => array("class" => "form-control", "placeholder" => "Entrez la CNI ou le numero de passport")))
                ->add('cniDate', "date", array("label" => "Date de delivrance de la CNI*", 'years' => range(date('Y') - 30, date('Y'))))
                ->add('cursu', 'collection', array("required" => false, 'label' => 'Cursus', 'type' => new \Blog\ArticleBundle\Form\cursusType(), 'allow_add' => true, 'allow_delete' => true, 'prototype' => true, 'attr' => array(
                        'class' => 'cursus',
                    ),))
                ->add('fDiplom', 'sonata_media_type', array("label" => "Photocopie non-certifiée du diplôme ou de l’attestation*",
                    'provider' => 'sonata.media.provider.file',
                    'context' => 'candidature',
                    "new_on_update" => true,
                ))
                ->add('fActe', 'sonata_media_type', array("label" => "Photocopie non-certifiée de l’acte de naissance*",
                    'provider' => 'sonata.media.provider.file',
                    'context' => 'candidature',
                    "new_on_update" => false,
                ))
                ->add('fCni', 'sonata_media_type', array("label" => " Photocopie non-certifiée de la CNI ou du PASSEPORT*",
                    'provider' => 'sonata.media.provider.file',
                    'context' => 'candidature'
                ))
                ->add('fPhoto', 'sonata_media_type', array("label" => " 2 Photos couleur 4 × 4*",
                    'provider' => 'sonata.media.provider.file',
                    'context' => 'candidature'
                ))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Blog\ArticleBundle\Entity\candid'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'blog_articlebundle_candid';
    }

}
