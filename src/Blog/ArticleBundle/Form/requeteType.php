<?php

namespace Blog\ArticleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class requeteType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email',"email",array("label"=>"Email*","attr"=>array("class"=>"form-control","placeholder"=>"Veuillez entrer votre email")))
            ->add('nom',"text",array("label"=>"Nom & prénom*","attr"=>array("class"=>"form-control","placeholder"=>"Veuillez entrer votre nom et votre prenom")))
            ->add('messages',"textarea",array("label"=>"Message*","max_length"=>180,"attr"=>array("class"=>"form-control","placeholder"=>"Veuillez saisir votre message")))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Blog\ArticleBundle\Entity\requete'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Message';
    }
}
