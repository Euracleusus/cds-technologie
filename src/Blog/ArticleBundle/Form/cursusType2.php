<?php

namespace Blog\ArticleBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class cursusType2 extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dDeb',"date",array("label"=>"De",'years' => range(date('Y')-30,date('Y'))))
            ->add('dFin',"date",array("label"=>"A",'years' => range(date('Y')-30,date('Y'))))
            ->add('etab',"text",array("label"=>"Etablissement*","attr"=>array("class"=>"form-control","placeholder"=>"Entrez l'etablissement fréquenté")))
            ->add('classe',"text",array("label"=>"Classe*","attr"=>array("class"=>"form-control","placeholder"=>"Entrez la classe")))
            ->add('filiere',"text",array("label"=>"Filiere*","attr"=>array("class"=>"form-control","placeholder"=>"Entrez la filiere")))
            ->add('diplome',"text",array("label"=>"Diplome obtenu*","attr"=>array("class"=>"form-control","placeholder"=>"Entrez le diplome")))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Blog\ArticleBundle\Entity\cursus'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'blog_articlebundle_cursus';
    }
}
