<?php

namespace Blog\ArticleBundle\Entity;
use ED\BlogBundle\Interfaces\Model\TaxonomyRelationInterface;
use ED\BlogBundle\Model\Entity\TaxonomyRelation as BaseTaxonomyRelation;
use Doctrine\ORM\Mapping as ORM;

/**
 * TaxonomyRelation
 *
 * @ORM\Table(name="taxonomyrelation")
 * @ORM\Entity()*/
class TaxonomyRelation extends BaseTaxonomyRelation implements TaxonomyRelationInterface
{
}
