<?php

namespace Blog\ArticleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Blog\ArticleBundle\Entity\user;
/**
 * article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity(repositoryClass="Blog\ArticleBundle\Entity\produitRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class produit {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="L'article doit avoir un nom")
     * @ORM\Column(name="name", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private $name;
    /**
     * @var string
     *
     * @ORM\Column(name="descr", type="text",nullable=true)
     */
    private $descr;
    

    /**
     * Reference de la piece
     * @var string
     * @Assert\NotBlank(message="L'article doit avoir une reference")
     * @ORM\Column(name="ref", type="string", length=255, precision=0, scale=0, nullable=false, unique=true)
     */
    private $ref;
     /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dCrea;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string
     * @ORM\Column(name="popu", type="float", precision=0, scale=0, nullable=false, unique=false)
     */
    private $popu;

    /**
     * @var string
     * @ORM\Column(name="vente", type="float", precision=0, scale=0, nullable=true, unique=false)
     */
    private $vente;

    /**
     * @var string

     * @Assert\NotBlank(message="L'article doit avoir un prix")
     * @ORM\Column(name="prix", type="float", precision=3,  nullable=true, unique=false)
     */
    private $prix;

    /**
     * Etat des stocks des pieces
     * @var string

     * @ORM\Column(name="stock", type="integer",  nullable=true, unique=false)
     */
    private $stock;

    //Devise du prix de l'article
    /**
     * @var \Blog\ArticleBundle\Entity\category
     *
     * @ORM\ManyToOne(targetEntity="Blog\ArticleBundle\Entity\category" , inversedBy="produit", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id",unique=false)
     * })
     * })
     */
    private $category;

   

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumn(name="imageD_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $imageD;
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumn(name="image1_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $image1;
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumn(name="image2_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $image2;

    

    const PRIX = 'prix';
    

    public function getTitle($nb) {
        $nb = (int) $nb;
        $nom = stripslashes($this->name);
        if (strlen($nom) > ($nb * 3)) {
            $nom = substr($this->$this->name, 0, ($nb * 3)) . "...";
        }

        return $nom;
    }

    function __toString() {
        return $this->getId() ? $this->getTitle(10) : "Nouveau produit"; // Assuming $this->getName() exists
    }

    public function getOtherPrice() {
        
    }

    public function getReduction() {
        return ($this->realPrice != 0 && $this->realPrice != null) ? round((100 - ($this->prix * 100 / $this->realPrice))) . "%" : null;
    }

       public function getPrice($type, $qte = 0, $format = true) {

            $prix = $this->prix;
            $prix = intval($qte) != 0 ? ($prix * $qte) : $prix;
        
        
       return \Blog\ArticleBundle\Controller\articleController::getPrice($prix,$this->getDev(), $format);
    }

    public function __construct() {
        $this->setPopu(0);
        $this->setStock(0);
        $this->setVente(0);
        $this->setRef("#ITS" . date("dmy") . strtoupper(substr(uniqid(sha1(time())), 0, 4)));
        $this->seuil = 5;
        $this->setImageD(null);
        $this->dCrea = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function resume() {
        return preg_replace('#^(.{13})(.*)#', '$1 .................................................', $this->contenu);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set aTitle
     *
     * @param string $aTitle
     * @return article
     */
    public function setATitle($aTitle) {
        $this->aTitle = $aTitle;

        return $this;
    }

    /**
     * Get aTitle
     *
     * @return string 
     */
    public function getATitle() {
        return $this->aTitle;
    }

    /**
     * Set dCrea
     *
     * @param \DateTime $dCrea
     * @return article
     */
    public function setDCrea($dCrea) {
        $this->dCrea = $dCrea;

        return $this;
    }

    /**
     * Get dCrea
     *
     * @return \DateTime 
     */
    public function getDCrea() {
        return $this->dCrea;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return article
     */
    public function setFile($file) {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile() {
        return $this->file;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return article
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set popu
     *
     * @param integer $popu
     * @return article
     */
    public function setPopu($popu) {
        $this->popu = $popu;

        return $this;
    }

    /**
     * Get popu
     *
     * @return integer 
     */
    public function getPopu() {
        return $this->popu;
    }

    /**
     * Set prix
     *
     * @param integer $prix
     * @return article
     */
    public function setPrix($prix) {
        $this->prix = $prix;

        if ($this->prix != null && $this->prix != $prix) {
            $aPrix = new mouvTarif();
            $aPrix->setPrix($prix);
            $aPrix->setArticle($this);
            $this->addAPrix($aPrix);
        }
        return $this;
    }

    /**
     * Get prix
     *
     * @return integer 
     */
    public function getPrix() {
        return $this->prix;
    }

    /**
     * Set dev
     *
     * @param \idieze\UserBundle\Entity\dev $dev
     * @return article
     */
    public function setDev(\idieze\UserBundle\Entity\dev $dev) {
        $this->dev = $dev;

        return $this;
    }

    /**
     * Get dev
     *
     * @return \idieze\UserBundle\Entity\dev 
     */
    public function getDev() {
        return $this->dev;
    }

    /**
     * Set etat
     *
     * @param \Blog\ArticleBundle\Entity\etat $etat
     * @return article
     */
    public function setEtat(\Blog\ArticleBundle\Entity\etat $etat) {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return \Blog\ArticleBundle\Entity\etat 
     */
    public function getEtat() {
        return $this->etat;
    }

    /**
     * Set proprio
     *
     * @param \idieze\UserBundle\Entity\User $proprio
     * @return article
     */
    public function setProprio(\idieze\UserBundle\Entity\User $proprio) {
        $this->proprio = $proprio;

        return $this;
    }

    /**
     * Get proprio
     *
     * @return \idieze\UserBundle\Entity\User 
     */
    public function getProprio() {
        return $this->proprio;
    }

    /**
     * Set category
     *
     * @param \Blog\ArticleBundle\Entity\category $category
     * @return article
     */
    public function setCategory(\Blog\ArticleBundle\Entity\category $category = null) {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Blog\ArticleBundle\Entity\category 
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * Add caract
     *
     * @param \Blog\ArticleBundle\Entity\caractart $caract
     * @return article
     */
    public function addCaract(\Blog\ArticleBundle\Entity\caractart $caract) {
        $this->caract[] = $caract;

        return $this;
    }

    /**
     * Remove caract
     *
     * @param \Blog\ArticleBundle\Entity\caractart $caract
     */
    public function removeCaract(\Blog\ArticleBundle\Entity\caractart $caract) {
        $this->caract->removeElement($caract);
    }

    /**
     * Get caract
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCaract() {
        return $this->caract;
    }

    /**
     * Add caractart
     *
     * @param \Blog\ArticleBundle\Entity\caractart $caractart
     * @return article
     */
    public function addCaractart(\Blog\ArticleBundle\Entity\caractart $caractart) {
        $this->caractart[] = $caractart;

        return $this;
    }

    /**
     * Remove caractart
     *
     * @param \Blog\ArticleBundle\Entity\caractart $caractart
     */
    public function removeCaractart(\Blog\ArticleBundle\Entity\caractart $caractart) {
        $this->caractart->removeElement($caractart);
    }

    /**
     * Get caractart
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCaractart() {
        return $this->caractart;
    }

    /**
     * Add caractart
     *
     * @param \Blog\ArticleBundle\Entity\caractart $caractart
     * @return article
     */

    /**
     * Add type
     *
     * @param \pc\VehiculeBundle\Entity\type $type
     * @return article
     */
    public function addType(\pc\VehiculeBundle\Entity\type $type) {
        $this->type[] = $type;

        return $this;
    }

    /**
     * Remove type
     *
     * @param \pc\VehiculeBundle\Entity\type $type
     */
    public function removeType(\pc\VehiculeBundle\Entity\type $type) {
        $this->type->removeElement($type);
    }

    /**
     * Get type
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set constr
     *
     * @param \pc\VehiculeBundle\Entity\constr $constr
     * @return article
     */
    public function setConstr(\pc\VehiculeBundle\Entity\constr $constr = null) {
        $this->constr = $constr;

        return $this;
    }

    /**
     * Get constr
     *
     * @return \pc\VehiculeBundle\Entity\constr 
     */
    public function getConstr() {
        return $this->constr;
    }

    /**
     * Set realPrice
     *
     * @param integer $realPrice
     * @return article
     */
    public function setRealPrice($realPrice) {
        $this->realPrice = $realPrice;

        return $this;
    }

    /**
     * Get realPrice
     *
     * @return integer 
     */
    public function getRealPrice() {
        return $this->realPrice;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     * @return article
     */
    public function setStock($stock) {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer 
     */
    public function getStock() {
        return $this->stock;
    }

    /**
     * Add souhait
     *
     * @param \Blog\ArticleBundle\Entity\souhait $souhait
     * @return article
     */
    public function addSouhait(\Blog\ArticleBundle\Entity\souhait $souhait) {
        $this->souhait[] = $souhait;

        return $this;
    }

    /**
     * Remove souhait
     *
     * @param \Blog\ArticleBundle\Entity\souhait $souhait
     */
    public function removeSouhait(\Blog\ArticleBundle\Entity\souhait $souhait) {
        $this->souhait->removeElement($souhait);
    }

    /**
     * Get souhait
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSouhait() {
        return $this->souhait;
    }

    /**
     * Set vente
     *
     * @param integer $vente
     *
     * @return article
     */
    public function setVente($vente) {
        $this->vente = $vente;

        return $this;
    }

    /**
     * Get vente
     *
     * @return integer
     */
    public function getVente() {
        return $this->vente;
    }

    /**
     * Set imageD
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $imageD
     *
     * @return article
     */
    public function setImageD(\Application\Sonata\MediaBundle\Entity\Media $imageD = null) {
        $this->imageD = $imageD;

        return $this;
    }

    /**
     * Get imageD
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getImageD() {
        return $this->imageD;
    }

    /**
     * Set ref
     *
     * @param string $ref
     *
     * @return article
     */
    public function setRef($ref) {
        $this->ref = $ref;

        return $this;
    }

    /**
     * Get ref
     *
     * @return string
     */
    public function getRef() {
        return $this->ref;
    }

    /**
     * Set exped
     *
     * @param \Blog\ArticleBundle\Entity\exped $exped
     *
     * @return article
     */
    public function setExped(\Blog\ArticleBundle\Entity\exped $exped = null) {
        $this->exped = $exped;

        return $this;
    }

    /**
     * Get exped
     *
     * @return \Blog\ArticleBundle\Entity\exped
     */
    public function getExped() {
        return $this->exped;
    }

    /**
     * Set seuil
     *
     * @param integer $seuil
     *
     * @return article
     */
    public function setSeuil($seuil) {
        $this->seuil = $seuil;

        return $this;
    }

    /**
     * Get seuil
     *
     * @return integer
     */
    public function getSeuil() {
        return $this->seuil;
    }

    /**
     * Add fournisseur
     *
     * @param \Blog\ArticleBundle\Entity\fournisseur $fournisseur
     *
     * @return article
     */
    public function addFournisseur(\Blog\ArticleBundle\Entity\fournisseur $fournisseur) {
        $this->fournisseur[] = $fournisseur;

        return $this;
    }

    /**
     * Remove fournisseur
     *
     * @param \Blog\ArticleBundle\Entity\fournisseur $fournisseur
     */
    public function removeFournisseur(\Blog\ArticleBundle\Entity\fournisseur $fournisseur) {
        $this->fournisseur->removeElement($fournisseur);
    }

    /**
     * Get fournisseur
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFournisseur() {
        return $this->fournisseur;
    }

    /**
     * Add mouv
     *
     * @param \Blog\ArticleBundle\Entity\mouv $mouv
     *
     * @return article
     */
    public function addMouv(\Blog\ArticleBundle\Entity\mouv $mouv) {
        $this->mouv[] = $mouv;

        return $this;
    }

    /**
     * Remove mouv
     *
     * @param \Blog\ArticleBundle\Entity\mouv $mouv
     */
    public function removeMouv(\Blog\ArticleBundle\Entity\mouv $mouv) {
        $this->mouv->removeElement($mouv);
    }

    /**
     * Get mouv
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMouv() {
        return $this->mouv;
    }

    /**
     * Add aPrix
     *
     * @param \Blog\ArticleBundle\Entity\mouvTarif $aPrix
     *
     * @return article
     */
    public function addAPrix(\Blog\ArticleBundle\Entity\mouvTarif $aPrix) {
        $this->aPrix[] = $aPrix;

        return $this;
    }

    /**
     * Remove aPrix
     *
     * @param \Blog\ArticleBundle\Entity\mouvTarif $aPrix
     */
    public function removeAPrix(\Blog\ArticleBundle\Entity\mouvTarif $aPrix) {
        $this->aPrix->removeElement($aPrix);
    }

    /**
     * Get aPrix
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAPrix() {
        return $this->aPrix;
    }

    /**
     * @ORM\PreUpdate
     */
    public function updateDate() {
        $this->setUpdatedAt(new \Datetime());
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return article
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * Add cmdFourQte
     *
     * @param \Blog\ArticleBundle\Entity\cmdFourQte $cmdFourQte
     *
     * @return article
     */
    public function addCmdFourQte(\Blog\ArticleBundle\Entity\cmdFourQte $cmdFourQte) {
        $this->cmdFourQte[] = $cmdFourQte;

        return $this;
    }

    /**
     * Remove cmdFourQte
     *
     * @param \Blog\ArticleBundle\Entity\cmdFourQte $cmdFourQte
     */
    public function removeCmdFourQte(\Blog\ArticleBundle\Entity\cmdFourQte $cmdFourQte) {
        $this->cmdFourQte->removeElement($cmdFourQte);
    }

    /**
     * Get cmdFourQte
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCmdFourQte() {
        return $this->cmdFourQte;
    }

    /**
     * Add promoRule
     *
     * @param \Blog\ArticleBundle\Entity\promoRules $promoRule
     *
     * @return article
     */
    public function addPromoRule(\Blog\ArticleBundle\Entity\promoRules $promoRule) {
        $this->promoRules[] = $promoRule;

        return $this;
    }

    /**
     * Remove promoRule
     *
     * @param \Blog\ArticleBundle\Entity\promoRules $promoRule
     */
    public function removePromoRule(\Blog\ArticleBundle\Entity\promoRules $promoRule) {
        $this->promoRules->removeElement($promoRule);
    }

    /**
     * Get promoRules
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPromoRules() {
        return $this->promoRules;
    }


    /**
     * Add cdeClient
     *
     * @param \Blog\ArticleBundle\Entity\venteClientQte $cdeClient
     *
     * @return article
     */
    public function addCdeClient(\Blog\ArticleBundle\Entity\venteClientQte $cdeClient)
    {
        $this->cdeClient[] = $cdeClient;

        return $this;
    }

    /**
     * Remove cdeClient
     *
     * @param \Blog\ArticleBundle\Entity\venteClientQte $cdeClient
     */
    public function removeCdeClient(\Blog\ArticleBundle\Entity\venteClientQte $cdeClient)
    {
        $this->cdeClient->removeElement($cdeClient);
    }

    /**
     * Get cdeClient
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCdeClient()
    {
        return $this->cdeClient;
    }

    /**
     * Set descr
     *
     * @param string $descr
     *
     * @return produit
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;

        return $this;
    }

    /**
     * Get descr
     *
     * @return string
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set image1
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $image1
     *
     * @return produit
     */
    public function setImage1(\Application\Sonata\MediaBundle\Entity\Media $image1 = null)
    {
        $this->image1 = $image1;

        return $this;
    }

    /**
     * Get image1
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getImage1()
    {
        return $this->image1;
    }

    /**
     * Set image2
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $image2
     *
     * @return produit
     */
    public function setImage2(\Application\Sonata\MediaBundle\Entity\Media $image2 = null)
    {
        $this->image2 = $image2;

        return $this;
    }

    /**
     * Get image2
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getImage2()
    {
        return $this->image2;
    }
    
    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateLien() {
        if ($this->getSlug() == null || $this->getSlug() == "")
            $this->setSlug(str_replace(' ', '-', $this->getName()));
    }

    public function getSlug() {
        return $this->slug;
    }
    /**
     * Set lien
     *
     * @param string $lien
     *
     * @return marque
     */
    public function setSlug($lien) {
        $this->slug = str_replace(' ', '_', $lien);

        return $this;
    }
}
