<?php

namespace Blog\ArticleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * address
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Blog\ArticleBundle\Entity\categoryRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="descr", type="text",nullable=true)
     */
    private $descr;
    /**
     * @var \idieze\PublishBundle\Entity\image
     * @ORM\OneToMany(targetEntity="Blog\ArticleBundle\Entity\produit",mappedBy="category", cascade={"persist","remove"})     
     */
    private $produit;
    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media")
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $image;
    
    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;
    
    
    function __toString() {
        return $this->getId() ? $this->getName() : "Nouvelle Categorie"; // Assuming $this->getName() exists
    }
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return slide
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set descr
     *
     * @param string $descr
     *
     * @return slide
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;

        return $this;
    }

    /**
     * Get descr
     *
     * @return string
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set image
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $image
     *
     * @return slide
     */
    public function setImage(\Application\Sonata\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getImage()
    {
        return $this->image;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->produit = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add produit
     *
     * @param \Blog\ArticleBundle\Entity\produit $produit
     *
     * @return category
     */
    public function addProduit(\Blog\ArticleBundle\Entity\produit $produit)
    {
        $this->produit[] = $produit;

        return $this;
    }

    /**
     * Remove produit
     *
     * @param \Blog\ArticleBundle\Entity\produit $produit
     */
    public function removeProduit(\Blog\ArticleBundle\Entity\produit $produit)
    {
        $this->produit->removeElement($produit);
    }

    /**
     * Get produit
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduit()
    {
        return $this->produit;
    }
    
    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateLien() {
        if ($this->getSlug() == null || $this->getSlug() == "")
            $this->setSlug(str_replace(' ', '-', $this->getName()));
    }

    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set lien
     *
     * @param string $lien
     *
     * @return marque
     */
    public function setSlug($lien) {
        $this->slug = str_replace(' ', '_', $lien);

        return $this;
    }

}
