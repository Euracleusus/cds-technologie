<?php

namespace Blog\ArticleBundle\Entity;

use ED\BlogBundle\Interfaces\Model\ArticleInterface;
use ED\BlogBundle\Model\Entity\Article as BaseArticle;
use Doctrine\ORM\Mapping as ORM;


/**
 * Article
 *
 * @ORM\Table(name="news")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="ED\BlogBundle\Model\Repository\ArticleRepository")
 */
class News extends BaseArticle implements ArticleInterface
{


    /**
     * Add category
     *
     * @param \Blog\ArticleBundle\Entity\Taxonomy $category
     *
     * @return News
     */
    public function addCategory(\Blog\ArticleBundle\Entity\Taxonomy $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \Blog\ArticleBundle\Entity\Taxonomy $category
     */
    public function removeCategory(\Blog\ArticleBundle\Entity\Taxonomy $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Add tag
     *
     * @param \Blog\ArticleBundle\Entity\Taxonomy $tag
     *
     * @return News
     */
    public function addTag(\Blog\ArticleBundle\Entity\Taxonomy $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param \Blog\ArticleBundle\Entity\Taxonomy $tag
     */
    public function removeTag(\Blog\ArticleBundle\Entity\Taxonomy $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Add metaDatum
     *
     * @param \Blog\ArticleBundle\Entity\ArticleMeta $metaDatum
     *
     * @return News
     */
    public function addMetaDatum(\Blog\ArticleBundle\Entity\ArticleMeta $metaDatum)
    {
        $this->metaData[] = $metaDatum;

        return $this;
    }

    /**
     * Remove metaDatum
     *
     * @param \Blog\ArticleBundle\Entity\ArticleMeta $metaDatum
     */
    public function removeMetaDatum(\Blog\ArticleBundle\Entity\ArticleMeta $metaDatum)
    {
        $this->metaData->removeElement($metaDatum);
    }
}
