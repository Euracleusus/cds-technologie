<?php

namespace Blog\ArticleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\ExecutionContextInterface;
use Blog\ArticleBundle\Validator\password;
use Blog\ArticleBundle\Validator\name;
use Blog\ArticleBundle\Validator\phone;
use FOS\UserBundle\Entity\User as BaseUser;
use ED\BlogBundle\Interfaces\Model\BlogUserInterface;
use ED\BlogBundle\Interfaces\Model\ArticleCommenterInterface;
/**
 * user
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Blog\ArticleBundle\Entity\userRepository")
 */
class user extends BaseUser implements BlogUserInterface, ArticleCommenterInterface{

  
      
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255,nullable=true)
     */
    private $nom;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255,nullable=true)
     */
    private $prenom;
    
    
  

 /**
     * Required by BlogUserInterface
     *
     * @ORM\Column(name="blog_display_name", type="string")
     */
    protected $blogDisplayName;

    public function getBlogDisplayName()
    {
        return $this->blogDisplayName;
    }

    public function setBlogDisplayName($blogDisplayName)
    {
        $this->blogDisplayName = $blogDisplayName;

        return $this;
    }

    public function getCommenterDisplayName()
    {
        return $this->blogDisplayName;
    }

    public function __sleep() {

        return array('id', 'username', 'password');
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }
    public function setUsername($username) {
        parent::setUsername($username);
        $this->setBlogDisplayName($username);
        $this->setEmail($username);
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->setBlogDisplayName("Pegasus");
    }
     public function getReduct()
    {
        $name= preg_replace('#^(\S+).*$#','$1',$this->fName);
        $surname= preg_replace('#^(\S+).*$#','$1',$this->lName);
        return $name.' '.$surname;
    }
   
   
    /**
     * Set solde
     *
     * @param integer $solde
     * @return user
     */
    public function setSolde($solde)
    {
        $this->solde = $solde;
    
        return $this;
    }

    /**
     * Get solde
     *
     * @return integer 
     */
    public function getSolde()
    {
        return $this->solde;
    }


    /**
     * Set fName
     *
     * @param string $fName
     * @return user
     */
    public function setFName($fName)
    {
        $this->fName = $fName;

        return $this;
    }

    /**
     * Get fName
     *
     * @return string 
     */
    public function getFName()
    {
        return $this->fName;
    }

    /**
     * Set lName
     *
     * @param string $lName
     * @return user
     */
    public function setLName($lName)
    {
        $this->lName = $lName;

        return $this;
    }

    /**
     * Get lName
     *
     * @return string 
     */
    public function getLName()
    {
        return $this->lName;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return user
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }



    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return user
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return user
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }
}
