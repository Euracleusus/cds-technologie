<?php

namespace Blog\ArticleBundle\Entity;
use ED\BlogBundle\Interfaces\Model\BlogTermInterface;
use ED\BlogBundle\Model\Entity\Term as BaseTerm;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Term
 ** @ORM\Table(name="term")
* @ORM\Entity()
 * @UniqueEntity("slug")
 */
class Term   extends BaseTerm implements BlogTermInterface
{
}
