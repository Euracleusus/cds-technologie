<?php

namespace Blog\ArticleBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Blog\ArticleBundle\Entity\user;
use ED\BlogBundle\Interfaces\Repository\BlogUserRepositoryInterface;
use ED\BlogBundle\Model\Repository\UserRepository as BaseUserRepository;
/**
 * userRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class userRepository extends BaseUserRepository implements BlogUserRepositoryInterface
{
     public function allAdminPage(user $user)
   {
       $qr=$this->createQueryBuilder('a')->where('a.id=:user')
               ->setParameter('user',$user->getId());
       $qr->getQuery()->getResult();
       
   }
   
   public function getDemandeAmis($user,$amis)
   {
       $qr=$this->createQueryBuilder('a')->leftJoin('a.demandeAmis','b')->where('a.id=:id and b.id=:aId')
               ->setParameter('id',$user->getId())
               ->setParameter('aId',$amis->getId());
       return $qr->getQuery()->getResult();
   }
   
   public function myFindAmis($user,$amis)
   {
       $qr=$this->createQueryBuilder('a')->leftJoin('a.amis','b')->where('a.id=:id and b.id=:aId')
               ->setParameter('id',$user->getId())
               ->setParameter('aId',$amis->getId());
       return $qr->getQuery()->getOneOrNullResult();
   }
   
   //recuperation de la liste des amis
   public function getAmis(user $user)
   {
       $qr=$this->createQueryBuilder('a')->leftJoin('a.amis','b')->where(' b.id=:id')->setMaxResults(20)
               ->setParameter('id',$user->getId());
       return $qr;
   }
   
   //recuperation des personnes ayant deja envoye un message a l'utilisateur
   
   public function myAllMsgFind(user $user){
       
       $qr=$this->createQueryBuilder('a')->leftJoin('a.messages','b')->leftJoin('b.author','c')->leftJoin('b.destinataires','d')->where('(c.id=:id or d.id=:id) AND a.id!=:id')->orderBy('b.date','DESC')->setMaxResults(10)
               ->setParameter('id',$user->getId())
               ;
       return $qr->getQuery()->getResult();
   }
    public function myfindSomeOne($recherchUs)
   {
        /*
         * @todo fonction de recherche a revoir
         */
       $qr=$this->createQueryBuilder('a')->where('a.fName LIKE :us ')->setMaxResults(5)
               ->setParameter('us',$recherchUs);
       $qr->getQuery()->getResult();
       
   }
 
   

}
