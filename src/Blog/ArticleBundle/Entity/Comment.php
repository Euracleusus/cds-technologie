<?php

namespace Blog\ArticleBundle\Entity;
use ED\BlogBundle\Interfaces\Model\CommentInterface;
use ED\BlogBundle\Model\Entity\Comment as BaseComment;
use Doctrine\ORM\Mapping as ORM;

/**
 * Comment
 *@ORM\HasLifecycleCallbacks
 * @ORM\Table(name="comment")
 @ORM\Entity(repositoryClass="ED\BlogBundle\Model\Repository\CommentRepository") */
class Comment extends BaseComment implements CommentInterface
{
}
