<?php

namespace Blog\ArticleBundle\Entity;


use ED\BlogBundle\Interfaces\Model\BlogTaxonomyInterface;
use ED\BlogBundle\Model\Entity\Taxonomy as BaseTaxonomy;
use Doctrine\ORM\Mapping as ORM;

/**
 * Taxonomy
 *
 * @ORM\Table(name="taxonomy")
 * @ORM\Entity(repositoryClass="ED\BlogBundle\Model\Repository\TaxonomyRepository")
*/
class Taxonomy extends BaseTaxonomy implements BlogTaxonomyInterface
{

    /**
     * Add child
     *
     * @param \Blog\ArticleBundle\Entity\Taxonomy $child
     *
     * @return Taxonomy
     */
    public function addChild(\Blog\ArticleBundle\Entity\Taxonomy $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \Blog\ArticleBundle\Entity\Taxonomy $child
     */
    public function removeChild(\Blog\ArticleBundle\Entity\Taxonomy $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Add article
     *
     * @param \Blog\ArticleBundle\Entity\News $article
     *
     * @return Taxonomy
     */
    public function addArticle(\Blog\ArticleBundle\Entity\News $article)
    {
        $this->articles[] = $article;

        return $this;
    }

    /**
     * Remove article
     *
     * @param \Blog\ArticleBundle\Entity\News $article
     */
    public function removeArticle(\Blog\ArticleBundle\Entity\News $article)
    {
        $this->articles->removeElement($article);
    }

    /**
     * Add tagged
     *
     * @param \Blog\ArticleBundle\Entity\News $tagged
     *
     * @return Taxonomy
     */
    public function addTagged(\Blog\ArticleBundle\Entity\News $tagged)
    {
        $this->tagged[] = $tagged;

        return $this;
    }

    /**
     * Remove tagged
     *
     * @param \Blog\ArticleBundle\Entity\News $tagged
     */
    public function removeTagged(\Blog\ArticleBundle\Entity\News $tagged)
    {
        $this->tagged->removeElement($tagged);
    }
}
