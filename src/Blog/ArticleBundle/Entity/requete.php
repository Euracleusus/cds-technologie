<?php

namespace Blog\ArticleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * address
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Blog\ArticleBundle\Entity\requeteRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class requete {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $dCrea;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;
    /**
     * @var string
     *
     * @ORM\Column(name="repondu", type="boolean", length=255)
     */
    private $repondu;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text",nullable=true)
     */
    private $messages;
    
    /**
     * @ORM\PreUpdate
     */
    public function updateDate() {
        $this->setUpdatedAt(new \Datetime());
    }
    
    public function __construct() {
      
        $this->dCrea = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->repondu = false;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dCrea
     *
     * @param \DateTime $dCrea
     *
     * @return requete
     */
    public function setDCrea($dCrea)
    {
        $this->dCrea = $dCrea;

        return $this;
    }

    /**
     * Get dCrea
     *
     * @return \DateTime
     */
    public function getDCrea()
    {
        return $this->dCrea;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return requete
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return requete
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return requete
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set messages
     *
     * @param string $messages
     *
     * @return requete
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;

        return $this;
    }

    /**
     * Get messages
     *
     * @return string
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set repondu
     *
     * @param boolean $repondu
     *
     * @return requete
     */
    public function setRepondu($repondu)
    {
        $this->repondu = $repondu;

        return $this;
    }

    /**
     * Get repondu
     *
     * @return boolean
     */
    public function getRepondu()
    {
        return $this->repondu;
    }
}
