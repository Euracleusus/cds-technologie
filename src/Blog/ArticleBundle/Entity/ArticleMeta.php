<?php

namespace Blog\ArticleBundle\Entity;

use ED\BlogBundle\Interfaces\Model\ArticleMetaInterface;
use ED\BlogBundle\Model\Entity\ArticleMeta as BaseArticleMeta;
use Doctrine\ORM\Mapping as ORM;
/**
 * ArticleMeta
 *
 * @ORM\Table(name="articlemeta")
 *  @ORM\Entity()
 */
class ArticleMeta extends BaseArticleMeta implements ArticleMetaInterface
{
   
}
